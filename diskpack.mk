# requires:
#    make v3.81 or higher
# must be loaded after binasm.mk

# where to locate the starter disk and SYS loader
LOADER_PATH		:= $(TEMPLATE_PATH)tools/loader
DISK_PATH		:= $(TEMPLATE_PATH)disks
AC_PATH			:= $(TEMPLATE_PATH)extern/ac/AppleCommander-ac-1.5.0.jar
BOOTSPOT_PATH	:= $(TEMPLATE_PATH)tools/bootspot

# grab start address from our linker config
# (using python because no shell under Windows)
BINSTART = $(shell python3 $(PYTHON_PATH)/extractstartaddr.py $(TARGET_MEMORY_MAP))

#$(warning DISK_TO_MAKE=$(DISK_TO_MAKE))
#$(warning DISK_PATH=$(DISK_PATH))
#$(warning DISK_VOLUME_TO_MAKE=$(DISK_VOLUME_TO_MAKE))
#$(warning MAKEFILE_LIST=$(MAKEFILE_LIST))

.PHONY: cleandsk startaddr makeprodos makeprodosbasic makeprodos35 makeprodosHDD makeblankprodos loadbin loadbin_selfstartprodos createbootspot

# builds a disk image w/ self-starting prodos
makeprodos:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, DISK_VOLUME_TO_MAKE, disk volume name)
	@:$(check_defined, DISK_TO_MAKE, disk image location)
	-cp -f $(DISK_PATH)/prodos-242.po $(DISK_TO_MAKE)
	$(AC) -n $(DISK_TO_MAKE) $(DISK_VOLUME_TO_MAKE)

makeprodosbasic:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, DISK_VOLUME_TO_MAKE, disk volume name)
	-cp -f $(DISK_PATH)/prodos-242-basic.po $(DISK_TO_MAKE)
	$(AC) -n $(DISK_TO_MAKE) $(DISK_VOLUME_TO_MAKE)

makeprodos35:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, DISK_VOLUME_TO_MAKE, disk volume name)
	-cp -f $(DISK_PATH)/prodos-242-35inch.po $(DISK_TO_MAKE)
	$(AC) -n $(DISK_TO_MAKE) $(DISK_VOLUME_TO_MAKE)

makeprodosHDD:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, DISK_VOLUME_TO_MAKE, disk volume name)
	-cp -f $(DISK_PATH)/harddisk-prodos-242.po $(DISK_TO_MAKE)
	$(AC) -n $(DISK_TO_MAKE) $(DISK_VOLUME_TO_MAKE)

# no prodos
makeblankprodos:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, DISK_VOLUME_TO_MAKE, disk volume name)
	-cp -f $(DISK_PATH)/prodos-242-blank.po $(DISK_TO_MAKE)
	$(AC) -n $(DISK_TO_MAKE) $(DISK_VOLUME_TO_MAKE)

# inserts bin w/ code
loadbin:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, BIN_TO_LOAD_START, hex address to load bin at)
	@:$(call check_defined, BIN_TO_LOAD, shortname of bin file)
	@:$(call check_defined, BIN_TO_LOAD_FULL, longname of bin file)
	$(AC) -p $(DISK_TO_MAKE) $(BIN_TO_LOAD) bin $(BIN_TO_LOAD_START) <$(BIN_TO_LOAD_FULL)

# inserts bin w/ code plus the cc65 boostrapper
loadbin_selfstartprodos:
	@:$(call check_defined, DISK_TO_MAKE, disk image filename)
	@:$(call check_defined, BIN_TO_LOAD_START, hex address to load bin at)
	@:$(call check_defined, BIN_TO_LOAD, shortname of bin file)
	@:$(call check_defined, BIN_TO_LOAD_FULL, longname of bin file)
	@echo Using start address of $(BIN_TO_LOAD_START)
	$(AC) -p $(DISK_TO_MAKE) $(BIN_TO_LOAD) bin $(BIN_TO_LOAD_START) <$(BIN_TO_LOAD_FULL)
	$(AC) -p $(DISK_TO_MAKE) $(BIN_TO_LOAD).SYSTEM sys 0x2000 <$(LOADER_PATH)/loader.system

createbootspot:
	@:$(call check_defined, BINS_TO_LOAD, comma seperated list of binfiles)
	@:$(call check_defined, BOOTSPOT_DST, bootspot system file to create)
	@echo Configuring Bootspot to use these bins: $(BINS_TO_LOAD)
	$(MAKE) -C $(BOOTSPOT_PATH) -f Bootspot.mk bin
	python3 $(BOOTSPOT_PATH)/src/bootspot-create.py --bins=$(BINS_TO_LOAD) --out=$(BOOTSPOT_DST)

startaddr:
	@echo Using start address of $(BINSTART)
