# requires:
#    make v3.81 or higher
#    BINFILE = name of the binfile to make
#    OBJS = list of objects to assemble
#    TARGET_ARCH = arch to compile for (apple2, apple2enh)

# configuration for paths to find stuff
TEMPLATE_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
TOOLS_PATH		:= $(TEMPLATE_PATH)tools
CC65_PATH		:= $(TEMPLATE_PATH)extern/cc65/bin
# extra, global includes for all source files
INCLUDE_PATH	:= $(TEMPLATE_PATH)include
LIBAPPLE_PATH	:= $(TEMPLATE_PATH)libapple
ASMCFG_PATH		:= $(TEMPLATE_PATH)asmconfig
PYTHON_PATH		:= $(TEMPLATE_PATH)python

define check_defined =
	$(strip $(foreach 1,$1, \
		$(call __check_defined,$1,$(strip $(value 2)))))
endef

define __check_defined =
	$(if $(value $1),, \
		$(error Undefined $1$(if $2, ($2))$(if $(value @), \
			required by target `$@')))
endef

ifeq ($(OS),Windows_NT)
	windows_path = $(subst /,\,$(1))
else
	windows_path = $(1)
endif

ifeq ($(OS),Windows_NT)
	make_path = $(shell mkdir $(call windows_path,$(1)))
else
	make_path = $(shell mkdir -p $(1))
endif


CPU_TYPE ?= 6502

OBJ_PATH	:= $(or $(OBJ_PATH),obj)
OBJS_PATH	= $(addprefix $(OBJ_PATH)/, $(OBJS)) $(addprefix $(OBJ_PATH)/, $(LIBS))

# compiler/linker flags
AFLAGS = -t $(TARGET_ARCH) -I $(call windows_path,$(INCLUDE_PATH)) -v -W1 --cpu $(CPU_TYPE)
#-W2
LFLAGS = -C $(TARGET_MEMORY_MAP) -v -vm -m $(BINFILE_PATH)/linkmap.txt -Ln $(BINFILE_PATH)/symbols.txt

# location of tools
ifeq ($(OS),Windows_NT)
	CA65 = $(CC65_PATH)/ca65.exe
	LD65 = $(CC65_PATH)/ld65.exe
else
	CA65 = $(CC65_PATH)/ca65
	LD65 = $(CC65_PATH)/ld65
endif
AC = java -jar $(AC_PATH)
SYMBOLCONV = $(PYTHON_PATH)/symbolconv.py

$(OBJ_PATH):
	$(call make_path, $@)

$(BINFILE_FULL): $(OBJS_PATH) $(TARGET_MEMORY_MAP)
	$(LD65) $(LFLAGS) $(OBJS_PATH) -o $(BINFILE_FULL)
	python $(SYMBOLCONV) $(BINFILE_PATH)/symbols.txt

$(OBJ_PATH)/apple2rom.o: $(LIBAPPLE_PATH)/apple2rom.s $(INCLUDE_PATH)/apple2rom.inc $(INCLUDE_PATH)/apple2rom-defs.inc $(INCLUDE_PATH)/utils.inc
	$(CA65) $(AFLAGS) $< -o $@

$(OBJ_PATH)/prodos.o: $(LIBAPPLE_PATH)/prodos.s $(INCLUDE_PATH)/prodos.inc $(INCLUDE_PATH)/prodos-snippets.inc $(INCLUDE_PATH)/prodos-defs.inc
	$(CA65) $(AFLAGS) $< -o $@

.DEFAULT_GOAL := all

.PHONY: bin cleanbin

bin: | $(OBJ_PATH) $(BINFILE_FULL)

cleanbin:
	-rm -f $(OBJ_PATH)/*.o $(BINFILE_FULL)
