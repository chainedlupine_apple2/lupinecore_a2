# requires:
#    make v3.81 or higher
# must be loaded after binasm.mk

# location of a few asset management tools
TOHGR				:= $(TOOLS_PATH)/tohgr/tohgr.exe
PALETTEDHGR			:= python3 $(TOOLS_PATH)/palettedhgr/palettedhgr.py
KR6502_KOMPRESSOR	:= python3 $(TOOLS_PATH)/kr6502/kompressor.py

