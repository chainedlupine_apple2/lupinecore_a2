# LupineCore (A2 edition)

My toolchain for building assembly code for Apple II development.

Library works with any Apple II from the Plus on up, and requires 64KB of RAM
and must run under ProDOS.

Note: I'm using Windows, and I've tried to make this system OS-agnostic but I
don't always test under Linux/etc so don't be surprised if it does not work.

# Functionality

Some built-in functionality:

* Just some makefiles, so easy to expand.
* Full support for ProDOS system and file operations.
* Supports running under Applesoft (via BASIC.SYSTEM)
* Supports HGR/DHGR graphics and includes a convertor that converts PNG files
* Built-in support for Oliver Schmidt's LOADER.SYSTEM
* Also includes BOOTSPOT loader, which has more features than LOADER.SYSTEM
* Can convert symbols into AppleWin format for easy debugging
* Disk packing/asset management (via AppleCommander)
* ProDOS shim code to open/load/manipulate files, CAT disks, detect BASIC, etc.
* Expands cc65's Apple ROM support with easy functions for detecting hardware, printing strings, copying memory between AUX/MAIN, etc.
* Plays nice with ProDOS so should work under GS/OS (untested)
* Based on ProDOS 2.4, so supports all Apple II hardware
* KOMPRESSOR 6502 library based on byte-pair encoding (around %50 space savings on text, %70 on graphics)

# Requirements

To use, download and install in the lupinecore_a2/extern directory:

1. [AppleCommander](https://github.com/AppleCommander) in extern/ac/
2. [cc65](https://github.com/cc65/cc65) in extern/cc65-snapshot-win32/
3. [tohgr](http://wsxyz.net/tohgr.html) in extern/tohgr/

Requires: python3

Also recommended: 
1. AppleWin for emulation (can install into extern and reference in makefiles)
2. ADTPro to transfer disks to real Apple A2 hardware

# BOOTSPOT

Check out the README for BOOTSPOT for more information about this boot shim code.

# KOMPRESSOR 6502

Check out the README for KOMPRESSOR 6502 for more information on how to use this library.

# PALETTEHGR

Check out the README for PALETTEHGR on how to convert .png files to A2 video-memory format graphics (.HGR/.DHGR)

# ProDOS shim

TODO for the shim code



