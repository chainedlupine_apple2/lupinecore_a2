import sys, os

# arg1 = pathname of source

sourcefn = sys.argv[1]
destfn = os.path.splitext(sourcefn)[0] + '_awin.sym'

with open(sourcefn, "r") as inputf:
  inputlines = inputf.readlines()

symb_count = 0

with open(destfn, "w") as outputf:
  for line in inputlines:
    addr = line[5:9]
    symbol = line[11:].rstrip()

    symbline = "SYM \"{}\" = {}".format(symbol, addr)
    #print (symbline)

    symb_count += 1

    outputf.write(symbline + "\n")


print("symbolconv: {} symbols converted".format(symb_count))
