; internal defines for ProDOS library
; can be used by itself if you just need access to names of common ProDOS memory locations/functions

.ifndef PDD_INC_GUARD
PDD_INC_GUARD := 1

MLI         := $BF00
;FILE_IO_BUFFER_ADDR  := MLI - $0400 ; using 0xBB00 as 1k buffer for ProDOS file IO

LAST_PATHNAME     := $0280

; OS stuff
CALL_QUIT           = $65
CALL_CREATE         = $C0
CALL_DESTROY        = $C1
CALL_RENAME         = $C2
CALL_SET_FILE_INFO  = $C3
CALL_GET_FILE_INFO  = $C4
CALL_ON_LINE        = $C5
CALL_SET_PREFIX     = $C6
CALL_GET_PREFIX     = $C7

; Filer stuff
CALL_OPEN       = $C8
CALL_READ       = $CA
CALL_WRITE      = $CB
CALL_CLOSE      = $CC

;.global DEVCNT, DEVLST, MACHID, RAMSLOT, NODEV

; ProDOS global system page constants
DEVCNT        := $BF31  ; GLOBAL PAGE DEVICE COUNT
DEVLST        := $BF32  ; GLOBAL PAGE DEVICE LIST
MACHID        := $BF98  ; GLOBAL PAGE MACHINE ID BYTE
RAMSLOT       := $BF26  ; SLOT 3, DRIVE 2 IS /RAM'S DRIVER VECTOR
;NODEV IS THE GLOBAL PAGE SLOT ZERO, DRIVE 1 DISK DRIVE VECTOR.
;IT IS RESERVED FOR USE AS THE "NO DEVICE CONNECTED" VECTOR.
NODEV         := $BF10
PREFIXSET     := $BF9A  ; 0 if no prefix is set (ie: GET_PREFIX would return null)
LASTDEVICE    := $bf30  ; unit_num of last device accessed by ProDOS

; system map
; to convert address to bitmap:
;   1. take page number, shift right three (divide by 8), this is index into bitmap bytes
;   2. take complement of bits 2-0, and this is bit in the bitmap byte to set (ie: page 0 is 000, complement 111, ergo bit 7 is set)
;     BIT  7    6     5     4     3     2     1     0 
;        +---------------------------------------------+
;        | Byte in Bit Map           |   Complement    |
; PAGE # | (only 0 through 23 valid) | of Bit in Byte  |
;        +---------------------------------------------+
BITMAP_PAGE_00_07 := $BF58  ; system map
BITMAP_PAGE_08_0F := $BF59  ; system map
BITMAP_PAGE_10_17 := $BF5A  ; system map
BITMAP_PAGE_18_1F := $BF5B  ; system map
BITMAP_PAGE_20_27 := $BF5C  ; system map
BITMAP_PAGE_28_2F := $BF5D  ; system map
BITMAP_PAGE_30_37 := $BF5E  ; system map
BITMAP_PAGE_38_3F := $BF5F  ; system map

BITMAP_PAGE_40_47 := $BF60  ; system map
BITMAP_PAGE_48_4F := $BF61  ; system map
BITMAP_PAGE_50_57 := $BF62  ; system map
BITMAP_PAGE_58_5F := $BF63  ; system map
BITMAP_PAGE_60_67 := $BF64  ; system map
BITMAP_PAGE_68_6F := $BF65  ; system map
BITMAP_PAGE_70_77 := $BF66  ; system map
BITMAP_PAGE_78_7F := $BF67  ; system map

BITMAP_PAGE_80_87 := $BF68  ; system map
BITMAP_PAGE_88_8F := $BF69  ; system map
BITMAP_PAGE_90_97 := $BF6A  ; system map
BITMAP_PAGE_98_9F := $BF6B  ; system map
BITMAP_PAGE_A0_A7 := $BF6C  ; system map
BITMAP_PAGE_A8_AF := $BF6E  ; system map
BITMAP_PAGE_B0_B7 := $BF6D  ; system map
BITMAP_PAGE_B8_BF := $BF6F  ; system map

; BASIC.SYSTEM global system page constants
BS_LASTSLOT   := $BE3C
BS_LASTDRIVE  := $BE3D


PRODOS_ERR_NONE               = $00
PRODOS_ERR_BAD_SYS_CALL_NUM   = $01  ; non-existent command num
PRODOS_ERR_BAD_SYS_CALL_PARAM = $02  ; same, but param was wrong
PRODOS_ERR_INTRPT_TABLE_FULL  = $25
PRODOS_ERR_IO                 = $27  ; hardware error with device
PRODOS_ERR_NO_DEVICE          = $28  ; device was not detected
PRODOS_ERR_WRITE_PROTECT      = $2B  ; device was write protected
PRODOS_ERR_DEVICE_SWITCHED    = $2E  ; attempt to interact to device, but no longer exists
PRODOS_ERR_BAD_PATHNAME       = $40  ; pathname has invalid characters
PRODOS_ERR_FCB_FULL           = $42  ; >8 files opened
PRODOS_ERR_INVALID_REF_NUM    = $43  ; REF_NUM for file is invalid
PRODOS_ERR_PATH_NOT_FOUND     = $44  ; path does not exist (but is valid)
PRODOS_ERR_VOLUME_NOT_FOUND   = $45  ; the volume name no longer exists (but is valid)
PRODOS_ERR_FILE_NOT_FOUND     = $46  ; the filename is not found (but is valid)
PRODOS_ERR_DUPLICATE_FILE     = $47  ; create or rename attempted on existing name
PRODOS_ERR_OVERRUN            = $48  ; out-of-space error
PRODOS_ERR_VOLUME_DIR_FULL    = $49  ; no more files can be created in volume directory
PRODOS_ERR_INCOMPAT_FORMAT    = $4A  ; incompatible file format
PRODOS_ERR_UNSUPPORTED_TYPE   = $4B  ; storage_type is unsupported
PRODOS_ERR_EOF                = $4C  ; EOF found in data stream
PRODOS_ERR_OUT_OF_RANGE       = $4D  ; requested position is >EOF
PRODOS_ERR_ACCESS             = $4E  ; access to operation (DESTROY, ERASE, etc) not permitted
PRODOS_ERR_FILE_OPEN          = $50  ; file is already open
PRODOS_ERR_DIR_COUNT          = $51  ; number of entriues in directory header does not match entries in file
PRODOS_ERR_NOT_PRODOS         = $52  ; not a ProDOS volume
PRODOS_ERR_INVALID_PARAM      = $53  ; invalid parameter in param block
PRODOS_ERR_VOL_FCB_FULL       = $55  ; volume control block table is full, >8 volumes
PRODOS_ERR_BAD_BUFFER_ADDRESS = $56  ; bad address for data/io buffer
PRODOS_ERR_DUPLICATE_VOLUME   = $57  ; two volumes have the same name
PRODOS_ERR_BITMAP_CORRUPT     = $5A  ; disk bitmap is corrupt/invalid

.endif


