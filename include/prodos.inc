; ProDOS routines
.segment "CODE_ROM_H"


.scope ProDOS

.include "prodos-defs.inc"

.global RAMDISK_DISCONNECT, RAMDISK_RECONNECT, RAMDISK_SET_FORMAT_BUFFER
.global DETECT_BASIC_SYSTEM, DETECT_PRODOS

; ------------------------------------------------
; -- STRUCTS for parameters
; ------------------------------------------------
.struct STRUCT_GET_FILE_INFO_PARAM
  PARAM_COUNT   .byte
  PATHNAME      .addr
  ACCESS        .byte
  FILE_TYPE     .byte
  AUX_TYPE      .word
  STORAGE_TYPE  .byte
  BLOCKS_USED   .word
  MOD_DATE      .word
  MOD_TIME      .word
  CREATE_DATE   .word
  CREATE_TIME   .word
.endstruct

.struct STRUCT_OPEN_PARAM
  PARAM_COUNT   .byte
  PATHNAME      .addr
  IO_BUFFER     .addr ; 1024 bytes required, must fall on page boundary (ie: multiple of $100)
  REF_NUM       .byte
.endstruct

.struct STRUCT_READ_PARAM
  PARAM_COUNT   .byte
  REF_NUM       .byte
  DATA_BUFFER   .addr
  REQUEST_COUNT .word
  TRANS_COUNT   .word
.endstruct

.struct STRUCT_CLOSE_PARAM
  PARAM_COUNT   .byte
  REF_NUM       .byte
.endstruct

.struct STRUCT_GETSET_PREFIX_PARAM
  PARAM_COUNT   .byte
  PATHNAME      .addr
.endstruct

.struct STRUCT_ON_LINE_PARAM
  PARAM_COUNT   .byte
  UNIT_NUM      .byte
  DATA_BUFFER   .addr
.endstruct

.struct STRUCT_CREATE_PARAM
  PARAM_COUNT   .byte
  PATHNAME      .addr
  ACCESS        .byte
  FILE_TYPE     .byte
  AUX_TYPE      .addr
  STORAGE_TYPE  .byte
  CREATE_DATE   .word
  CREATE_TIME   .word
.endstruct

.struct STRUCT_WRITE_PARAM
  PARAM_COUNT   .byte
  REF_NUM       .byte
  DATA_BUFFER   .addr
  REQUEST_COUNT .word
  TRANS_COUNT   .word
.endstruct

.struct STRUCT_DESTROY_PARAM
  PARAM_COUNT   .byte
  PATHNAME      .addr
.endstruct

.endscope

; ------------------------------------------------
; -- PROCs for STRUCTS
; ------------------------------------------------

.macro ProDOS_DEFINE_DESTROY_PARAM
  .byte $01 ; 1 params
  .RES .sizeof(ProDOS::STRUCT_DESTROY_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_GETSET_PREFIX_PARAM
  .byte $01 ; 1 params
  .RES .sizeof(ProDOS::STRUCT_GETSET_PREFIX_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_ON_LINE_PARAM
  .byte $02 ; 2 params
  .RES .sizeof(ProDOS::STRUCT_ON_LINE_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_GET_FILE_INFO_PARAM
  .byte $0A ; 11 params
  .RES .sizeof(ProDOS::STRUCT_GET_FILE_INFO_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_OPEN_PARAM iobuffer_addr
  .byte $03 ; 3 params
  .addr $0000
  .if .blank (iobuffer_addr)
    .byte $00
    .byte $00
  .else
    .byte <iobuffer_addr
    .byte >iobuffer_addr
  .endif

  .byte $00
.endmacro

.macro ProDOS_DEFINE_QUIT_PARAM
  .byte   $04             ;PARAM_COUNT
  .byte   $00             ;QUIT_TYPE
  .word   $0000           ;RESERVED
  .byte   $00             ;RESERVED
  .word   $0000           ;RESERVED
.endmacro

.macro ProDOS_DEFINE_READ_PARAM
  .byte $04       ;PARAM_COUNT
  .RES .sizeof(ProDOS::STRUCT_READ_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_WRITE_PARAM
  .byte $04       ;PARAM_COUNT
  .RES .sizeof(ProDOS::STRUCT_WRITE_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_CLOSE_PARAM
  .byte $01       ;PARAM_COUNT
  .RES .sizeof(ProDOS::STRUCT_CLOSE_PARAM) - 1, $00
.endmacro

.macro ProDOS_DEFINE_CREATE_PARAM
  .byte $07       ;PARAM_COUNT
  .RES .sizeof(ProDOS::STRUCT_CREATE_PARAM) - 1, $00
.endmacro

; ------------------------------------------------
; -- MACROS for MLI functions
; ------------------------------------------------

.macro PRDS_DEBUG_NOPS
    .if .defined(DEBUG_PRODOS)
      .repeat 5
      nop
      .endrepeat
    .endif
.endmacro


.macro ProDOS_DESTROY param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_DESTROY
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_SET_PREFIX param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_SET_PREFIX
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_ON_LINE param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_ON_LINE
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_QUIT param_addr
    INC $3F4
    JSR ProDOS::MLI
    .byte ProDOS::CALL_QUIT
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_OPEN param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_OPEN
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_READ param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_READ
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_WRITE param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_WRITE
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_CLOSE param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_CLOSE
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_GET_PREFIX param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_GET_PREFIX
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro

.macro ProDOS_CREATE param_addr
    JSR ProDOS::MLI
    .byte ProDOS::CALL_CREATE
    .word param_addr
    PRDS_DEBUG_NOPS
.endmacro