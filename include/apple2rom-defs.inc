; internal defines for ROM routines


.feature c_comments

;.out "checking APPLE2ROMS-DEFS "
.ifndef RMD_INC_GUARD
RMD_INC_GUARD := 1

;.out "including APPLE2ROMS-DEFS "


; machine types
AT_IIorig   := $01
AT_IIplus   := $02
AT_IIe      := $03
AT_IIeEnh   := $04
AT_IIc      := $05
AT_III      := $06

MT_48k      := $01
MT_64k      := $02
MT_128k     := $03




ROM_HOME    := $FC58    ; clear screen, homes cursor
ROM_COUT    := $FDED    ; print char in a
ROM_CROUT   := $FD8E    ; print CR
ROM_PRBYTE  := $FDDA    ; prints hex in a
ROM_HEX     := $FDE3    ; prints lower nibble as hex, in a
ROM_PRNTAX  := $F941    ; prints hex, x=lo, a=hi
ROM_PRBL2   := $F94A    ; prints blanks, x=num to sned (0=256)
ROM_WAIT    := $FCA8    ; waits, a=seconds to wait, formula: delay=(0.5*(5*(dly*dly)+27*dly+26))/10000
ROM_GETKEY  := $FD0C    ; get a key
ROM_RDKEY   := $FD0C    ; get a key

ROM_WNDLFT  := $20
ROM_WNDWDTH := $21
ROM_WNDTOP  := $22
ROM_WNDBTM  := $23

ROM_NORMAL  := $FE84
ROM_INVERSE := $FE80

ROM_CH      := $24      ; cursor x
ROM_CV      := $25      ; cursor y (must move cursor if set)

ROM_CURDOWN   := $FC66  ; move cursor down, etc
ROM_CURUP     := $FC1A
ROM_CURLEFT   := $FC10
ROM_CURRIGHT  := $FBF4

ROM_VTAB      := $FC22  ; VERTICAL SCREEN TAB USING CV 
ROM_VTABA     := $FC24  ; VERTICAL SCREEN TAB USING A 

ROM_CLREOP    := $FC42  ; CLEAR TO END OF PAGE
ROM_CLREOL    := $FC9C  ; clears to the end of line (from current cursor pos)

ROM_SCROLL    := $FC70  ; Scroll text screen up

ROM_MACHID		:= $BF96

ROM_INTAUXMOVE		:= $C311


ROM_PWRUP   := $FAA6    ; Cold start
ROM_RESET   := $FA62    ; Warm start

MEM_HGR_PAGE1	:= $2000
MEM_HGR_PAGE2	:= $4000

; soft switches (apple //e)
SW_80STOREOFF  	:= $C000    ; Allow PAGE1/2 to switch between video pagel and page2 (read: key in)
SW_80STOREON	  := $C001		; Allow PAGE1/2 to switch between main and aux. video memory
SW_RDMAINRAM	  := $C002    ; If 80STORE Off: Read Main Mem $0200-$BFFF
SW_RDCARDRAM 	  := $C003		; If 80STORE Off: Read Aux Mem $0200-$BFFF
SW_WRMAINRAM 	  := $C004  	; If 80STORE Off: Write Main Mem $0200-$BFFF
SW_WRCARDRAM 	  := $C005		; If 80STORE Off: Write Aux Mem $0200-$BFFF
SW_INTCROMOFF   := $C006    ; Enable slot ROM for $C100-$CFFF
SW_INTCROMON    := $C007    ; Enable main ROM for $C100-$CFFF
SW_ALTZPOFF     := $C008    ; If set, use main for SP/ZP
SW_ALTZPON      := $C009    ; If set, use main for SP/ZP
SW_KEYSTROBE	  := $C010		; reset keyboard strobe (W)
SW_SETINTC3ROM	:= $C00A		; Select ROM in Slot 3 ($C300-$C3FF)
SW_SETSLOTC3ROM := $C00B    ; Select ROM in Aux Slot ($C300-$C3FF)
SW_80COLOFF    	:= $C00C		; Turn off 80-col display hardware
SW_80COLON 		  := $C00D		; Turn on 80-col display hardware
SW_ALTCHAROFF   := $C00E    ; Disable alternate character set
SW_ALTCHARON    := $C00F    ; Enable alternate character set
SW_SPKR         := $C030    ; Toggle speaker
SW_GR		  	    := $C050		; Graphics mode
SW_TEXT			    := $C051		; Text mode
SW_FULL  		    := $C052		; Fullscreen graphics
SW_MIXED  		  := $C053		; Mixed graphics
SW_PAGE1	 	    := $C054		; Select page1 display (or main video memory)
SW_PAGE2		    := $C055		; Select page2 display (or aux video memory)
SW_LORES		    := $C056		; Select LORES
SW_HIRES	   	  := $C057		; Select HIRES
SW_SETAN3  		  := $C05E		; Turn on annunciator 3 on game-port IO (enable double-width mode)
SW_CLRAN3		    := $C05F		; Turn off annunciator 3 on game-port IO (disable double-width mode)


; RAM switches
SW_LCBANK2R     := $C080    ; Swap in LC bank 2, RO
SW_ROMIN        := $C081    ; Swap in D000-FFFF ROM, write LC bank 2
SW_ROMINR       := $C082    ; Swap in D000-FFFF ROM, no writes
SW_LCBANK2RW    := $C083    ; Swap in LC bank 2, RW
SW_LCBANK1RW    := $C08B    ; Swap in LC bank 1, RW
SW_LCBANK1R     := $C08C    ; Swap in LC bank 1, RO


; flags
SB_KEYB         := $C010    ; write to strobe (clear) keyboard matrix buffer
IN_KEYB         := $C000    ; read for key waiting in matrix buffer

FL_RDLCBNK2     := $C011   ; >127 if LC bank 2 in use
FL_RDLCRAM      := $C012   ; >127 if LC is read enabled
FL_RAMRD        := $C013    ; if >=$80, reading from AUX, otherwise MAIN
FL_RAMWR        := $C014    ; if >=$80, writing from AUX, otherwise MAIN
FL_ALTZP        := $C016    ; if >=$80, using AUX ZP/SP, otherwise MAIN
FL_PAGE2		    := $C01C		; is page2 on (or aux selected)

; zeropage temps
A1L			  := $3C
A1H   		:= $3D

A2L			  := $3E
A2H   		:= $3F

A3L			  := $40
A3H   		:= $41

A4L			  := $42
A4H   		:= $43

ARNDL     := $50
ARNDH     := $51

STACK     := $0100
VERSION		:= $FBB3

;.global GETKEY
;.global GFX_MODE_DHGR, GFX_DHGR_PAGE1, GFX_DHGR_PAGE2
;.global GFX_MODE_DHGR_MIXED, PRNG
;.global TEXT_MODE_40
;.global _ROM_PRINT, _ROM_PRINT_CINV, _MEMCOPY, _MEMFILL
;.global WAIT_VBLANK, MOD8


.scope KB

kb_left_arrow     = $08
kb_right_arrow    = $15
kb_up_arrow       = $0b
kb_down_arrow     = $0a
kb_escape         = $1b
kb_space          = $20
kb_enter          = $0D
kb_delete         = $7F
kb_plus           = $3D
kb_minus          = $2D

kb_A              = $41
kb_B              = $42
kb_C              = $43
kb_D              = $44
kb_E              = $45
kb_F              = $46
kb_G              = $47
kb_H              = $48
kb_I              = $49
kb_J              = $4A
kb_K              = $4B
kb_L              = $4C
kb_M              = $4D
kb_N              = $4E
kb_O              = $4F
kb_P              = $50
kb_Q              = $51
kb_R              = $52
kb_S              = $53
kb_T              = $54
kb_U              = $55
kb_V              = $56
kb_W              = $57
kb_X              = $58
kb_Y              = $59
kb_Z              = $5A

kb_LEFTBRACKET    = $5B 
kb_RIGHTBRACKET   = $5D

kb_a              = $61
kb_b              = $62
kb_c              = $63
kb_d              = $64
kb_e              = $65
kb_f              = $66
kb_g              = $67
kb_h              = $68
kb_i              = $69
kb_j              = $6A
kb_k              = $6B
kb_l              = $6C
kb_m              = $6D
kb_n              = $6E
kb_o              = $6F
kb_p              = $70
kb_q              = $71
kb_r              = $72
kb_s              = $73
kb_t              = $74
kb_u              = $75
kb_v              = $76
kb_w              = $77
kb_x              = $78
kb_y              = $79
kb_z              = $7A


kb_0              = $30
kb_1              = $31
kb_2              = $32
kb_3              = $33
kb_4              = $34
kb_5              = $35
kb_6              = $36
kb_7              = $37
kb_8              = $38
kb_9              = $39

.endscope

.endif