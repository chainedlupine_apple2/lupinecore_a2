; ------------------------------------------------
; -- UTILITY macros
; ------------------------------------------------
.feature addrsize

		; places addr into a, addr+1 into x
		; addr can be either immediate or addr
.macro Util_LOAD_AX_Addr addr
		.local @argvalue
		.if (.match (.left (1, {addr}), #))
			@argvalue = .right(.tcount(addr)-1, addr)
			.byte $A2  ; LDX #imm
			.byte .hibyte (@argvalue)
			.byte $A9  ; LDA #imm
			.byte .lobyte (@argvalue)
		.else
			ldx addr+1
			lda addr
		.endif
.endmacro

		; places val into a
		; addr can be either immediate or addr
.macro Util_LOAD_A_BVal val
		.local @argvalue
		.if (.match (.left (1, {val}), #))
			@argvalue = .right(.tcount(val)-1, val)
			.byte $A9  ; LDA #imm
			.byte .lobyte (@argvalue)
		.else
			LDA val
		.endif
.endmacro

; Move symbol/memory into some memory location
; append with # to do immediate loading
.macro Util_LOAD src, dst
	Util_LOAD_AX_Addr src
	STA 	dst
	STX		dst+1
.endmacro

; PASCAL-like string (first byte is length)
.macro Util_LSTR str
	.byte .strlen(str), str
.endmacro

.macro Util_Inc_16_Addr addr, amt
	.if .blank(amt) 	; increment by 1 if nothing given w/ rollover
		inc addr
		bne :+
		inc addr+1
	:
	.else 					 	; increment by amt
			; do 8-bit addition w/ rollover
			lda addr
			clc
			adc amt
			bcc :+
			inc addr+1
		:
			sta addr
	.endif
.endmacro

.macro Util_Inc_16_Addr_16_Lit addr, amt
		; do 16-bit addition w/ rollover
		clc
		lda addr
		adc #<amt
		sta addr
		lda addr+1
		adc #>amt
		sta addr+1
.endmacro

.macro Util_Inc_16_Addr_16_Addr addr, amt
		; do 16-bit addition w/ rollover
		clc
		lda addr
		adc amt
		sta addr
		lda addr+1
		adc amt+1
		sta addr+1
.endmacro

; takes 16-bit addr and increments it (by amt, if given)
;.macro Util_Inc_16_Addr_new addr, amt
;	.if .blank(amt) 	; increment by 1 if nothing given w/ rollover
;		inc addr
;		bne :+
;		inc addr+1
;	:
;	.else 					 	; increment by amt
;		.if .addrsize(amt) = 1
;				; do 8-bit addition w/ rollover
;				lda addr
;				clc
;				adc amt
;				bcc :+
;				inc addr+1
;			:
;				sta addr
;		.else
;				; do 16-bit addition w/ rollover
;				clc
;				lda addr
;				adc amt
;				sta addr
;				lda addr+1
;				adc amt+1
;				sta addr+1
;		.endif
;	.endif
;.endmacro

.macro Util_Dec_16_Addr addr, amt
	.if .blank(amt) 	; decrement by 1
		lda addr
		bne :+					; only dec the hibyte if lo is 0
		dec addr+1
	:
		dec addr
	.else 					 	; decrement by amt
		lda addr
		sec
		sbc amt
		bcs :+
		dec addr+1
	:
		sta addr
	.endif
.endmacro

; sets carry if at zero
.macro Util_16_Addr_IsZero addr
		clc
		lda addr+1
		bne :+
		lda addr
		bne :+
		sec
	:
.endmacro

.macro Util_Inc_16_Addr_Struct addr, struct
		lda addr
		clc
		;.byte $69 
		;.byte .sizeof (struct)
		adc #.sizeof(struct)
		bcc :+
		inc addr+1
	:
		sta addr
.endmacro


.macro Util_Dec_16_Addr_Struct addr, struct
		lda addr
		sec
		adc #.sizeof(struct)
		bcs :+
		dec addr+1
	:
		sta addr
.endmacro


.macro SUB label
	.export label
	.proc label
.endmacro

.macro END_SUB
	.endproc
.endmacro

.macro DEFDATA label
	.export label
	.proc label
.endmacro

.macro END_DEFDATA
	.endproc
.endmacro

.macro PUSH_16 addr
	.local @argvalue
	.if (.match (.left (1, {addr}), #))
		@argvalue = .right(.tcount(addr)-1, addr)
		.byte $A9  ; LDA #imm
		.byte .hibyte (@argvalue)
		pha
		.byte $A9  ; LDA #imm
		.byte .lobyte (@argvalue)
		pha
	.else
		lda addr
		pha
		lda addr+1
		pha
	.endif
.endmacro


.macro POP_16 addr
	pla
	sta addr+1
	pla
	sta addr
.endmacro