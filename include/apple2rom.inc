; Apple 2 ROM helper routines

.scope ROM

IGNORE_SCOPE = 1
.include "apple2.inc"
.include "apple2rom-defs.inc"

.global TEXT_MODE_40, _ROM_PRINT, _ROM_PRINT_CINV, _MEMCOPY, _MEMFILL, GFX_MODE_DHGR, GFX_MODE_DHGR_MIXED, GFX_DHGR_PAGE1, WAIT_VBLANK, _ROM_PRINT_CINV_LINES, _ROM_LSTR_PRINT
.global MOD8, PRNG, PRNG_2, machine_type, DETECT_APPLE_TYPE, DETECT_APPLE_MEMORY, machine_ram
.global GFX_MODE_HGR, GFX_HGR_PAGE1, GFX_HGR_PAGE2

; used by the PHX/PHY simulators for non 65c02 processors
temp_a = $90
temp_x = $91
temp_y = $92

;.global temp_a, temp_x, temp_y

.macro ROM_PRINT addr
		Util_LOAD_AX_Addr addr
		jsr ROM::_ROM_PRINT
.endmacro

.macro ROM_PRINT_CINV addr
		Util_LOAD_AX_Addr addr
		jsr ROM::_ROM_PRINT_CINV
.endmacro

.macro ROM_PRINT_CINV_LINES addr
		Util_LOAD_AX_Addr addr
		jsr ROM::_ROM_PRINT_CINV_LINES
.endmacro

.macro ROM_LSTR_PRINT addr
		Util_LOAD_AX_Addr addr
		jsr ROM::_ROM_LSTR_PRINT
.endmacro


.enum   EAUXMOVE_MODE
                TO_MAIN	= 0
                TO_AUX	= 1
        .endenum

.macro ROM_AUXMOVE dstaddr, srcaddr, size, toaux
		.if (toaux = 1)
			SEC
		.else
			CLC
		.endif

		Util_LOAD_AX_Addr srcaddr
		STA ROM::A1L
		STX ROM::A1H

		LDA #<(srcaddr + size)
		STA ROM::A2L
		LDA #>(srcaddr + size)
		STA ROM::A2H

		Util_LOAD_AX_Addr dstaddr
		STA ROM::A4L
		STX ROM::A4H

		JSR ROM::ROM_INTAUXMOVE
.endmacro

.macro ROM_MEMCOPY dstaddr, srcaddr, size
		Util_LOAD_AX_Addr dstaddr
		STA ROM::A1L
		STX ROM::A1H

		Util_LOAD_AX_Addr srcaddr
		STA ROM::A2L
		STX ROM::A2H

		Util_LOAD_AX_Addr size
		STA ROM::A3L
		STX ROM::A3H

		JSR ROM::_MEMCOPY
.endmacro

.macro ROM_MEMFILL dstaddr, val, size
		Util_LOAD_AX_Addr dstaddr
		STA ROM::A1L
		STX ROM::A1H

		Util_LOAD_A_BVal val
		STA ROM::A2L

		Util_LOAD_AX_Addr size
		STA ROM::A3L
		STX ROM::A3H

		JSR ROM::_MEMFILL
.endmacro

.macro U_PHX
  .ifpc02
    phx
  .else
    sta ROM::temp_a
    txa
    pha
    lda ROM::temp_a
  .endif
.endmacro

.macro U_PLX
  .ifpc02
    plx
  .else
    sta ROM::temp_a
    pla
    tax
    lda ROM::temp_a
  .endif
.endmacro

.macro U_PHY
  .ifpc02
    phx
  .else
    sta ROM::temp_a
    tya
    pha
    lda ROM::temp_a
  .endif
.endmacro

.macro U_PLY
  .ifpc02
    plx
  .else
    sta ROM::temp_a
    pla
    tay
    lda ROM::temp_a
  .endif
.endmacro

.macro U_PHXY
  .ifpc02
    phx
    phy
  .else
    sta ROM::temp_a
    txa
    pha
    tya
    pha
    lda ROM::temp_a
  .endif
.endmacro

.macro U_PLXY
  .ifpc02
    plx
    ply
  .else
    sta ROM::temp_a
    pla
    tax
    pla
    tay
    lda ROM::temp_a
  .endif
.endmacro

.macro U_DEC
  .ifpc02
    dec a
  .else
    php
    sec
    sbc #1
    plp
  .endif
.endmacro

.macro U_INC
  .ifpc02
    inc a
  .else
    php
    clc
    adc #1
    plp
  .endif
.endmacro


.endscope