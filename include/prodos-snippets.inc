; macros which define ProDOS utility functions
; note: If using ProDOS library, these will be implemented in prodos.o
; only include this file directly if you just intend to use parts of this code in your own ProDOS shim code
.FEATURE addrsize

INST_JMP   := $4c

.macro Snippet_OPEN_FILE in_pathname, in_open_param
    Util_LOAD in_pathname, in_open_param+ProDOS::STRUCT_OPEN_PARAM::PATHNAME

    ProDOS_OPEN in_open_param
.endmacro

.macro Snippet_COPY_PARAM_REFNUM in_open_param, in_read_param, in_write_param, in_close_param
    LDA   in_open_param+ProDOS::STRUCT_OPEN_PARAM::REF_NUM
    STA   in_read_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
    STA   in_write_param+ProDOS::STRUCT_READ_PARAM::REF_NUM
    STA   in_close_param+ProDOS::STRUCT_CLOSE_PARAM::REF_NUM
.endmacro

; params:
;   in_pathname, .word      = lstr pathname
;   in_create_param, struct = struct for create params
.macro Snippet_FILE_CREATE in_pathname, in_create_param
    Util_LOAD in_pathname, in_create_param+ProDOS::STRUCT_CREATE_PARAM::PATHNAME

    lda   #$C3 ; destroy, rename, write, read enabled
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::ACCESS

    lda   #$7F ; general binary file
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::FILE_TYPE

    lda   #$00
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::AUX_TYPE
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::AUX_TYPE+1
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_DATE
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_DATE+1
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_TIME
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::CREATE_TIME+1

    lda   #$01 ; standard file
    sta   in_create_param+ProDOS::STRUCT_CREATE_PARAM::STORAGE_TYPE

    ProDOS_CREATE in_create_param
.endmacro

; params:
;   in_databuffer, .word  = where to write to
;   in_datasize, .word    = amount to read
.macro Snippet_FILE_WRITE in_databuffer, in_datasize, in_write_param
    Util_LOAD in_databuffer, in_write_param+ProDOS::STRUCT_WRITE_PARAM::DATA_BUFFER
    Util_LOAD in_datasize, in_write_param+ProDOS::STRUCT_WRITE_PARAM::REQUEST_COUNT

    ProDOS_WRITE in_write_param
    bcs   write_error

    ; verify we wrote the amount requested
    lda   in_datasize
    cmp   in_write_param+ProDOS::STRUCT_WRITE_PARAM::TRANS_COUNT
    bne   :+
    lda   in_datasize+1
    cmp   in_write_param+ProDOS::STRUCT_WRITE_PARAM::TRANS_COUNT+1
    bne   :+
      lda   #0 ; all good
      clc
      jmp   write_error
  : ; error on write
    lda   #ProDOS::PRODOS_ERR_OVERRUN
    sec
  write_error:
    ; error, just straight outta here
.endmacro

.macro Snippet_FILE_READ in_databuffer, in_datasize, in_read_param
    Util_LOAD in_databuffer, in_read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
    Util_LOAD in_datasize, in_read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT

    ProDOS_READ in_read_param
    bcs   done

    ; verify we read the amount requested
    lda   in_datasize
    cmp   in_read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT
    bne   :+
    lda   in_datasize+1
    cmp   in_read_param+ProDOS::STRUCT_READ_PARAM::TRANS_COUNT+1
    bne :+
      lda   #0 ; all good
      clc
      jmp   done
  : ; error on read
    lda   #ProDOS::PRODOS_ERR_EOF
    sec

    ; error, just leave
  done:
.endmacro

.macro Snippet_FILE_DESTROY in_pathname, in_destroy_param
    Util_LOAD in_pathname, in_destroy_param+ProDOS::STRUCT_DESTROY_PARAM::PATHNAME
    ProDOS_DESTROY in_destroy_param
.endmacro

.macro Snippet_SET_PREFIX in_pathname, in_getset_prefix_param
    Util_LOAD in_pathname, in_getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
    ProDOS_SET_PREFIX in_getset_prefix_param
.endmacro


.macro Snippet_GET_PREFIX in_pathname, in_getset_prefix_param
    Util_LOAD in_pathname, in_getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
    ProDOS_GET_PREFIX in_getset_prefix_param

.endmacro


; Get our current path info.  So if we're booting from fresh, then we might not have a valid PREFIX that tells us where to look
; for files.  This might result in errors, so we need to do some detective work to figure out where we are running from.

; Possibilities:
; 1. We launched from a sub-directory (either in the ProDOS launcher or BASIC.SYSTEM).  Prefix will already be set, so done.
; 2. We booted from a diskette (or ProFiler root, etc) into ProDOS. Prefix will be set, so done.
; 3. We booted from a diskette (or ProFiler etc) into BASIC.SYSTEM.  Prefix will not be set.  But we can get our current unitnum
;    from the BASIC global page, then ON_LINE to get volume name, and SET_PREFIX.

; This will not only set the current path into ProDOS:LAST_PATHNAME ($280) but also populate the current slot/drive unitnum.
; You can choose to use/ignore the pathname in LAST_PATHNAME as you desire.

.macro Snippet_GET_PATH_INFO in_curr_unitnum, in_getset_prefix_param, in_on_line_param
  .scope
    tempvar = temp1
    ; first, check to see if we have a prefix.  If so, then go get it.
    lda   ProDOS::PREFIXSET
    beq   :+
      ; grab curr unitnum from ProDOS
      lda   ProDOS::LASTDEVICE
      sta   in_curr_unitnum
      jmp get_prefix
  :

    ; no prefix, we need to set it.  So!  First off...  Look for BASIC.SYSTEM!
    Snippet_DETECT_BASIC_SYSTEM
    bcc   no_basic

    ; we are inside the BASIC.SYSTEM sys, so we need to do a bit extra logic to figure out the current boot volume
    lda   ProDOS::BS_LASTSLOT
    asl
    asl
    asl
    asl             ; convert 0X to X0 for slot number
    sta   tempvar
    lda   ProDOS::BS_LASTDRIVE
    sec
    sbc   #1        ; convert 1-index to 0-index
    clc
    ror
    bcc   :+
    ora   #$80
  :
    ora   tempvar   ; combine with shifted slot number
    sta   in_curr_unitnum
    jmp   online_scan

  no_basic:
    ; no BASIC, so we will just use the last device accessed by ProDOS for our ON_LINE source
    lda   ProDOS::LASTDEVICE
    sta   in_curr_unitnum

  online_scan:
    ; Get volume str via ON_LINE command into LAST_PATHNAME
    Util_LOAD #ProDOS::LAST_PATHNAME+1, in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    lda   in_curr_unitnum
    sta   in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM

    ProDOS_ON_LINE in_on_line_param
    bcs   done    ; we're done, w/ error

    ; LAST_PATHNAME should now contain our volume name (if freshly booted) or volname+path (if starting from a directory)

    ; load in the length returned from ON_LINE, and mask out the len byte
    lda   ProDOS::LAST_PATHNAME+1
    and   #$0F      ; mask off the high nibble just leaving volume length
    clc
    adc   #1        ; add one, since we need a / at the beginning as ON_LINE does not return it
    sta   ProDOS::LAST_PATHNAME
    lda   #$2F      ; '/'
    sta   ProDOS::LAST_PATHNAME+1

    clc
    jmp   done

  get_prefix:
    ; request prefix from ProDOS (this will be full pathname, volname+path)
    Util_LOAD #ProDOS::LAST_PATHNAME, in_getset_prefix_param+ProDOS::STRUCT_GETSET_PREFIX_PARAM::PATHNAME
    ProDOS_GET_PREFIX in_getset_prefix_param
    bcs   done ; we had an error

    ; we don't need to modify this pathname like we do for the ON_LINE command, so we're good

  done:   ; likely with error!
  .endscope
.endmacro

; Scan all volumes and write them to in_databuffer (must be 256 bytes available)
.macro Snippet_DISK_ALL_ON_LINE in_databuffer, in_on_line_param
    Util_LOAD in_databuffer, in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    ProDOS_ON_LINE in_on_line_param
.endmacro

; writes current volume name to ProDOS::LAST_PATHNAME (with necessary / at beginning)
.macro Snippet_GET_VOLUME_NAME in_curr_unitnum, in_on_line_param
  .scope
    temp_len  = temp1

    ; force just our boot device to rescan its ProDOS volume
    Util_LOAD #ProDOS::LAST_PATHNAME+1, in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    lda   in_curr_unitnum
    sta   in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM
    ProDOS_ON_LINE in_on_line_param
    bcc   :+
      ; ON_LINE command problem, error will be in accum
      jmp   done
  :

    ; alright, ON_LINE executed, now let's see if there is an error in the actual returned volume name record
    ; first check for error
    lda   ProDOS::LAST_PATHNAME+1
    and   #$0F ; mask off first nibble bits (ignore drive/slot in last nibble)
    bne   :+ ; if the length is zero, then there was an error
      ; zero, so error
      lda   ProDOS::LAST_PATHNAME+2   ; ProDOS error code, stored in second byte of record
      sec
      jmp   done
  : ; if not zero, then we're fine, we got a volume name

    sta   temp_len
    ; now turn into a proper prefix path (ie: including prefix /)
    lda   #$2F
    sta   ProDOS::LAST_PATHNAME+1 ; first write $2F to front of path
    lda   temp_len
    sta   ProDOS::LAST_PATHNAME   ; and write trimmed len to first byte
    inc   ProDOS::LAST_PATHNAME   ; adding 1, because we need to add the prefix /
    clc

  done:
  .endscope
.endmacro


; Resync a volume name.  We are given in_curr_path, which is an LSTR of the current prefix.
; Do an ON_LINE on in_curr_unitnum to get current volume name, and compare it to in_curr_path.  If it differs, a
; new volume has been inserted.
; assumes in_curr_path has a prefix / in front of it
; returns: carry set if new volume, ProDOS::LAST_PATHNAME contains new volume name (with / prepended)
;          carry clear, no new volume.  a>0 = any ProDOS errors
.macro Snippet_RESYNC_VOLUME_NAME in_curr_path, in_curr_unitnum, in_on_line_param, in_getset_prefix_param
  .scope
    temp_len  = temp1
    temp_char = temp1

    lda   in_curr_unitnum
    cmp   #$ff
    bne   :+
    jmp   vol_name_error
  :

    ; load volume name into LAST_PATHNAME
    Snippet_GET_VOLUME_NAME in_curr_unitnum, in_on_line_param
    bcc   :+
    ; error, so set accordingly, error should be in a
    clc
    rts
  :

    ; now that we've done that, let's compare it to our current path.  If the first part is not the same, then this is a new
    ; volume and we will set the carry
    ldx   ProDOS::LAST_PATHNAME
  cmp_loop:
    lda   ProDOS::LAST_PATHNAME, x
    cmp   in_curr_path, x
    bne   cmp_failed
    dex
    bne   cmp_loop
    jmp   no_cmp_error

  cmp_failed:
    ; there was a match, so this must be a new volume
    sec
    lda   #0 ; no error
    jmp   done

  no_cmp_error:
    ; we compared all bytes of pathname, and it all matched
    clc
    lda   #0 ; no error
    jmp   done

  vol_name_error:
    ; something wrong w/ volume, just return
    lda #$FF
    clc
    jmp   done

  done:
  .endscope
.endmacro

; use ON_LINE to get all active volumes on the machine, and compare to in_curr_path.  If found,
; set in_curr_unitnum to this volume slot/drive.
; returns: carry if found, in_curr_unitnum updated
;          not carry = error, a is error
; TODO
.macro Snippet_FIND_VOLUME_NAME in_databuffer, in_curr_path, in_curr_unitnum, in_on_line_param
  .scope
    Util_LOAD in_databuffer, in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::DATA_BUFFER
    lda   #0  ; scan all volumes
    sta   in_on_line_param+ProDOS::STRUCT_ON_LINE_PARAM::UNIT_NUM
    ProDOS_ON_LINE in_on_line_param
    ; So we should now have all mounted devices


  .endscope

.endmacro

.macro Snippet_GET_CURR_UNITNUM in_curr_unitnum
  .scope
    lda   ProDOS::LASTDEVICE
    sta   in_curr_unitnum
  .endscope
.endmacro

; assumes templstr has a prefix / in front of it
.macro Snippet_LSTR_TO_PATH templstr, in_curr_path
  .scope
    temp_len  = temp1

    ldx   #0
    lda   #0
  clear_loop:
    sta   in_curr_path, x
    inx
    cpx   #$40
    bcc   clear_loop

    ; check length
    ldy   #0
    lda   (templstr), y
    bne   :+
      ; error, so let's set this to just a slash
      lda   #1
      sta   in_curr_path
      lda   #$2F
      sta   in_curr_path+1
      jmp   done
  :

    ldx   #0
    ldy   #0
    lda   (templstr), y
    sta   temp_len
    ldy   #1
  loop_char:
    lda   (templstr), y
    sta   in_curr_path+1,x
    iny
    inx
    cpx   temp_len
    bne   loop_char

    ; check to see if there is a / at end, if not, then add it
    lda   in_curr_path,x
    cmp   #$2F
    beq   already_terminated
    ; add a / to the end
    inx
    lda   #$2F
    sta   in_curr_path,x

    ; increment length to indicate 1+ path
    inc   temp_len

  already_terminated:
    ; copy over the original size
    lda   temp_len
    sta   in_curr_path

  done:
  .endscope
.endmacro

.macro Snippet_GET_CATALOG dirBlock, callback, in_curr_prefix, in_dir_load_size, in_open_param, in_read_param, in_close_param
  .scope
    entryPtr        = temp1w
    fileCount       = temp2w
    activeEntries   = temp3w
    entryLength     = temp1
    entriesPerBlock = temp2
    blockEntries    = temp3

    lda   callback
    sta   patchup_callback+1
    lda   callback+1
    sta   patchup_callback+2

    Util_LOAD in_curr_prefix, in_open_param+ProDOS::STRUCT_OPEN_PARAM::PATHNAME
    ProDOS_OPEN in_open_param
    bcc   :+
      jmp   done
    :

    jsr   FILE_PREPARE

    ; read first 512 bytes
    Util_LOAD arg1w, in_read_param+ProDOS::STRUCT_READ_PARAM::DATA_BUFFER
    lda   #<in_dir_load_size
    sta   in_read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT
    lda   #>in_dir_load_size
    sta   in_read_param+ProDOS::STRUCT_READ_PARAM::REQUEST_COUNT+1

    ProDOS_READ in_read_param
    bcc   :+
      jmp   done
    :

    ; alright, start the scan
    ldy   #$23
    lda   (dirBlock), y
    sta   entryLength

    ldy   #$24
    lda   (dirBlock), y
    sta   entriesPerBlock

    ldy   #$25
    lda   (dirBlock), y
    sta   fileCount

    ldy   #$26
    lda   (dirBlock), y
    sta   fileCount+1

    Util_LOAD dirBlock, entryPtr

    ; begin scan past directory header
    lda   entryLength
    clc
    adc   #$4
    sta   temp4
    Util_Inc_16_Addr entryPtr, temp4

    lda   #2
    sta   blockEntries

    lda   #0
    sta   activeEntries
    sta   activeEntries+1

  dirloop:
    ldy   #0
    lda   (entryPtr), y
    ; if zero, skip
    beq   invalidEntry
      ; process entry by printing it out
      ZP_SAVE
      Util_LOAD entryPtr, arg1w
    patchup_callback:
      jsr   $0000
      ZP_RESTORE

      Util_Inc_16_Addr activeEntries, #1
  invalidEntry:


    ; 16-unsigned compare on activeEntries < fileCount
    lda   activeEntries+1
    cmp   fileCount+1
    bcs   :+
      jmp   activeLessThan
    :
    bne   fileGreaterThan
    lda   activeEntries
    cmp   fileCount
    bcs   :+
      jmp   activeLessThan
    :
    jmp   fileGreaterThan
  activeLessThan:
    ; activeEntries < fileCount, so more entries to process
    lda   blockEntries
    cmp   entriesPerBlock
    beq   loadNextBlock
      ; we are not done, so continue next entry
      Util_Inc_16_Addr entryPtr, entryLength
      inc   blockEntries
      jmp   dirloop
  loadNextBlock:
    ; our current block is done, so read next
    ProDOS_READ in_read_param
    bcc   :+
      jmp   done
    :
    ; reset entryPtr back to top of block, then skip 4 bytes
    Util_LOAD dirBlock, entryPtr
    lda   #$4
    sta   temp4
    Util_Inc_16_Addr entryPtr, temp4

    lda   #$1
    sta   blockEntries

;           LDA NUM1H  ; compare high bytes
;           CMP NUM2H
;           BCC LABEL2 ; if NUM1H < NUM2H then NUM1 < NUM2
;           BNE LABEL1 ; if NUM1H <> NUM2H then NUM1 > NUM2 (so NUM1 >= NUM2)
;           LDA NUM1L  ; compare low bytes
;           CMP NUM2L
;           BCC LABEL2 ; if NUM1L < NUM2L then NUM1 < NUM2
;    LABEL1

  fileGreaterThan:
    ; we've processed all entries, so just stop

    ProDOS_CLOSE in_close_param

  done:
  .endscope
.endmacro

; sets carry if ProDOS
.macro Snippet_DETECT_PRODOS
  .scope
    sec
    lda   $bf00
    cmp   #INST_JMP
    bne   err
    lda   $bf09
    cmp   #INST_JMP
    bne   err
    lda   $bf0c
    cmp   #INST_JMP
    bne   err
    jmp   exit
  err:
    clc
  exit:
  .endscope
.endmacro

; sets carry if BASIC.SYSTEM detected
.macro Snippet_DETECT_BASIC_SYSTEM
  .scope
    sec
    ; first, check to see if page below $bf00 is reserved
    lda   $bf6f
    and   $40
    beq   err

    ; confirm jump points
    lda   $be00
    cmp   #INST_JMP
    bne   err
    lda   $be03
    cmp   #INST_JMP
    bne   err
    lda   $be06
    cmp   #INST_JMP
    bne   err
    lda   $be09
    cmp   #INST_JMP
    bne   err
    lda   $be0c
    cmp   #INST_JMP
    bne   err
    jmp   exit
  err:
    clc
  exit:
  .endscope
.endmacro

; requires:
; .byte RAMUNITID
; .word RAM_ADDR
.macro Snippet_RAM_DISCONNECT RAM_ADDR, RAMUNITID
      PHP                     ; SAVE STATUS AND
      SEI                     ; MAKE SURE INTERRUPTS ARE OFF!

      ; FIRST THING TO DO IS TO SEE IF THERE IS A /RAM TO DISCONNECT!

      LDA ProDOS::MACHID      ; LOAD THE MACHINE ID BYTE
      AND #$30                ; TO CHECK FOR A 128k SYSTEM
      CMP #$30                ; IS IT 128k?
      BNE @DONE               ; IF NOT THEN BRANCH SINCE NO /RAM!

      LDA ProDOS::RAMSLOT     ; IT IS 128K; IS A DEVICE THERE?
      CMP ProDOS::NODEV       ; COMPARE WITH LOW BYTE OF NODEV
      BNE @CONT               ; BRANCH IF NOT EQUAL, DEVICE IS CONNECTED
      LDA ProDOS::RAMSLOT+1   ; CHECK HI BYTE FOR MATCH
      CMP ProDOS::NODEV+1     ; ARE WE CONNECTED?
      BEQ @DONE               ; BRANCH, NO WORK TO DO; DEVICE NOT THERE

    ; AT THIS POINT /RAM (OR SOME OTHER DEVICE) IS CONNECTED IN
    ; THE SLOT 3, DRIVE 2 VECTOR.  NOW WE MUST GO THRU THE DEVICE
    ; LIST AND FIND THE SLOT 3, DRIVE 2 UNIT NUMBER OF /RAM ($BF).
    ; THE ACTUAL UNIT NUMBERS, (THAT IS TO SAY 'DEVICES') THAT WILL
    ; BE REMOVED WILL BE $BF, $BB, $B7, $B3.  /RAM'S DEVICE NUMBER
    ; IS $BF.  THUS THIS CONVENTION WILL ALLOW OTHER DEVICES THAT
    ; DO NOT NECESSARILY RESEMBLE (OR IN FACT, ARE COMPLETELY DIFFERENT
    ; FROM) /RAM TO REMAIN INTACT IN THE SYSTEM.

  @CONT:
      LDY ProDOS::DEVCNT      ; GET THE NUMBER OF DEVICES ONLINE
  @LOOP:
      LDA ProDOS::DEVLST,Y    ; START LOOKING FOR /RAM OR FACSIMILE
      AND #$F3                ; LOOKING FOR $BF, $BB, $B7, $B3
      CMP #$B3                ; IS DEVICE NUMBER IN {$BF,$BB,$B7,$B3}?
      BEQ @FOUND              ; BRANCH IF FOUND..
      DEY                     ; OTHERWISE CHECK OUT THE NEXT UNIT #.
      BPL @LOOP               ; BRANCH UNLESS YOU'VE RUN OUT OF UNITS.
      BMI @DONE               ; SINCE YOU HAVE RUN OUT OF UNITS TO
  @FOUND:
      LDA ProDOS::DEVLST,Y    ; GET THE ORIGINAL UNIT NUMBER BACK
      STA RAMUNITID           ; AND SAVE IT OFF FOR LATER RESTORATION.

    ; NOW WE MUST REMOVE THE UNIT FROM THE DEVICE LIST BY BUBBLING
    ; UP THE TRAILING UNITS.
  @GETLP:
      LDA ProDOS::DEVLST+1,Y  ; GET THE NEXT UNIT NUMBER
      STA ProDOS::DEVLST,Y    ; AND MOVE IT UP.
      BEQ @EXIT               ; BRANCH WHEN DONE(ZEROS TRAIL THE DEVLST)
      INY                     ; CONTINUE TO THE NEXT UNIT NUMBER...
      BNE @GETLP              ; BRANCH ALWAYS.
  @EXIT:
      LDA ProDOS::RAMSLOT     ; SAVE SLOT 3, DRIVE 2 DEVICE ADDRESS.
      STA RAM_ADDR            ; SAVE OFF LOW BYTE OF /RAM DRIVER ADDRESS
      LDA ProDOS::RAMSLOT+1   ; SAVE OFF HI BYTE
      STA RAM_ADDR+1          ;

      LDA ProDOS::NODEV       ; FINALLY COPY THE 'NO DEVICE CONNECTED'
      STA ProDOS::RAMSLOT     ; INTO THE SLOT 3, DRIVE 2 VECTOR AND
      LDA ProDOS::NODEV+1     ;
      STA ProDOS::RAMSLOT+1   ;
      DEC ProDOS::DEVCNT      ; DECREMENT THE DEVICE COUNT.
  @DONE:  PLP                 ; RESTORE STATUS
      CLI
.endmacro

; requires:
; .byte RAMUNITID
; .word RAM_ADDR
; .word FORMAT_BUFFER (normally $2000)
.macro Snippet_RAM_RECONNECT RAMUNITID, RAM_ADDR, FORMAT_BUFFER
      PHP                     ; SAVE STATUS
      SEI                     ; AND MAKE SURE INTERRUPTS ARE OFF!

      LDA RAM_ADDR
      BNE @GOOD_ADDR          ; IF RAM_ADDR=$0000, THEN ABORT.
      LDA RAM_ADDR+1
      BNE @GOOD_ADDR
      JMP @DONE1
  @GOOD_ADDR:
      LDY ProDOS::DEVCNT      ; GET THE NUMBER OF DEVICES - 1.
  @LOOP1:
      LDA ProDOS::DEVLST,Y    ; LOAD THE UNIT NUMBER
      AND #$F0                ; CHECK FOR SLOT 3, DRIVE 2 UNIT.
      CMP #$B0                ; IS IT THE SLOT 3, DRIVE 2 UNIT?
      BEQ @DONE1              ; IF SO BRANCH.
      DEY                     ; OTHERWISE SEARCH ON...
      BPL @LOOP1              ; LOOP UNTIL DEVLST SEARCH IS COMPLETED
      LDA RAM_ADDR            ; RESTORE THE DEVICE DRIVER ADDRESS
      STA ProDOS::RAMSLOT     ; LOW BYTE..
      LDA RAM_ADDR+1          ; NOW THE
      STA ProDOS::RAMSLOT+1   ; HI BYTE.
      INC ProDOS::DEVCNT      ; AFTER INSTALLING DEVICE, INC DEVICE COUNT
      LDY ProDOS::DEVCNT      ; USE Y FOR LOOP COUNTER..
  @LOOP2:
      LDA ProDOS::DEVLST-1,Y  ; BUBBLE DOWN THE ENTRIES IN DEVICE LIST
      STA ProDOS::DEVLST,Y    ;
      DEY                     ; NEXT
      BNE @LOOP2              ; LOOP UNTIL ALL ENTRIES MOVED DOWN.

    ; NOW SET UP A /RAM FORMAT REQUEST

      LDA #3                  ; LOAD ACC WITH FORMAT REQUEST NUMBER.
      STA $42                 ; STORE REQUEST NUMBER IN PROPER PLACE.

      LDA RAMUNITID           ; RESTORE THE DEVICE
      STA ProDOS::DEVLST      ; UNIT NUMBER IN THE DEVICE LIST
      AND #$F0                ; STRIP THE DEVICE ID (ZERO LOW NIBBLE)
      STA $43                 ; AND STORE THE UNIT NUMBER IN $43.

      LDA FORMAT_BUFFER       ; LOAD LOW BYTE OF BUFFER POINTER
      STA $44                 ; AND STORE IT.
      LDA FORMAT_BUFFER+1     ; LOAD HI BYTE OF BUFFER POINTER
      STA $45                 ; AND STORE IT.

      LDA $C08B               ; READ & WRITE ENABLE
      LDA $C08B               ; THE LANGUAGE CARD WITH Read/write RAM bank 1

    ; NOTE HOW THE DRIVER IS CALLED.  YOU JSR TO AN INDIRECT JMP SO
    ; CONTROL IS RETURNED BY THE DRIVER TO THE INSTRUCTION AFTER THE JSR.

      JSR @DRIVER             ; NOW LET DRIVER CARRY OUT CALL.
      BIT $C082               ; NOW PUT ROM BACK ON LINE.

      BCC @DONE1              ; IF THE CARRY IS CLEAR --> NO ERROR
      JSR @ERROR              ; GO PROCESS THE ERROR

  @DONE1:
      PLP                     ; RESTORE STATUS
      CLI
      RTS                     ; THAT'S ALL

  @DRIVER:
      JMP (ProDOS::RAMSLOT)   ; CALL THE /RAM DRIVER

  @ERROR:
      ;BRK                    ; YOUR ERROR HANDLER CODE WOULD GO HERE
      CLI
.endmacro