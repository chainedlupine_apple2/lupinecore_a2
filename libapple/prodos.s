; ProDOS routines (implementation)

.segment "CODE_ROM_H"

.scope ProDOS

.include "prodos.inc"
.include "prodos-snippets.inc"
.include "prodos-defs.inc"

.export DEVCNT, RAMSLOT, NODEV, MACHID, DEVLST
.global RAMDISK_DISCONNECT, RAMDISK_RECONNECT, RAMDISK_SET_FORMAT_BUFFER
.global DETECT_BASIC_SYSTEM, DETECT_PRODOS

DETECT_BASIC_SYSTEM:
      Snippet_DETECT_BASIC_SYSTEM
      rts

DETECT_PRODOS:
      Snippet_DETECT_PRODOS
      rts

RAMDISK_DISCONNECT:
      Snippet_RAM_DISCONNECT RAM_ADDR, RAMUNITID
      rts

RAMDISK_RECONNECT:
      Snippet_RAM_RECONNECT RAM_ADDR, RAMUNITID, FORMAT_BUFFER
      rts

; a, x = lo, hi to buffer
RAMDISK_SET_FORMAT_BUFFER:
    sta FORMAT_BUFFER
    stx FORMAT_BUFFER+1
    rts

.export RAM_ADDR, RAMUNITID

.segment "DATA_H"
  FORMAT_BUFFER:
    .word $0020			; $2000 by default
  RAM_ADDR: 	
    .word $0000     ; STORE THE DEVICE DRIVER ADDRESS HERE
  RAMUNITID: 	
    .byte $00     	; STORE THE DEVICE'S UNIT NUMBER HERE	


.endscope

