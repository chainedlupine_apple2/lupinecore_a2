.scope ROM

.segment "CODE_ROM_H"

.include "apple2rom.inc"

		JMP ROM::SOFTEV	; do a soft reboot if we get here

.global TEXT_MODE_40, _ROM_PRINT, _ROM_PRINT_CINV, _MEMCOPY, _MEMFILL, GFX_MODE_DHGR, GFX_MODE_DHGR_MIXED, GFX_DHGR_PAGE1, WAIT_VBLANK, _ROM_PRINT_CINV_LINES, _ROM_LSTR_PRINT
.global MOD8, PRNG, PRNG_2, machine_type, DETECT_APPLE_TYPE, DETECT_APPLE_MEMORY, machine_ram
.global GFX_MODE_HGR, GFX_HGR_PAGE1, GFX_HGR_PAGE2


; uses COUT to print an ASCII string (not HICASE)
_ROM_PRINT:
        sta     ROM::A1L
        stx     ROM::A1H
        ldx     ROM::VERSION
        ldy     #$00
:       lda     (ROM::A1L),y
        beq     :++							; jump if we're done
        cpx     #$06            ; Are we a //e or c?  If not, skip handling converting lowercase
        beq     :+
        cmp     #$60            ; lowercase ?
        bcc     :+
        and     #$5F            ; -> uppercase
:       ora     #$80						; set hibit and output
        jsr     ROM::ROM_COUT
        iny
        bne     :--             ; Branch always
: 			rts		

; uses COUT to print an ASCII string (prints INVERSE if hi bit is set)
_ROM_PRINT_CINV:
        sta     ROM::A1L
        stx     ROM::A1H
				lda 		#$80
				sta 		ROM::A2L
        ldx     ROM::VERSION
        ldy     #$00
:       lda     (ROM::A1L),y
        beq     :++++
        cpx     #$06            ; Are we a //e or c?  If not, skip handling converting lowercase
        beq     :+
        cmp     #$60            ; lowercase ?
        bcc     :+
        and     #$5F            ; -> uppercase
:       
				bit			ROM::A2L
				bne			:+
				ora     #$80
				jsr			ROM::ROM_COUT
				jmp			:++
:			
				; we don't need to ora it by 128, as it's already got the highbit set
				U_PHY 		; save y because INVERSE/NORMAL trashes it
				jsr			ROM::ROM_INVERSE
				jsr			ROM::ROM_COUT
				jsr			ROM::ROM_NORMAL
				U_PLY
:
        iny
        bne     :----             ; Branch always
: 		rts		

; uses COUT to print an ASCII string (prints INVERSE if hi bit is set)
_ROM_PRINT_CINV_LINES:
        sta     ROM::A1L
        stx     ROM::A1H
				; get our current cursor X and save it
				lda 		ROM::ROM_CH
				sta 		ROM::A3L

				lda 		#$80
				sta 		ROM::A2L
        ldx     ROM::VERSION
        ldy     #$00
@loop:
       	lda     (ROM::A1L),y
        beq     @endstring
				cmp 		#$0D
				bne 		@noenter
				; enter found, so we need to reset
				U_PHY
				jsr 		ROM::ROM_CURDOWN
				lda 		ROM::A3L
				sta			ROM::ROM_CH
				U_PLY
				jmp 		@next

@noenter:
        cpx     #$06            ; Are we a //e or c?  If not, skip handling converting lowercase
        beq     @noconversion
        cmp     #$60            ; lowercase ?
        bcc     @noconversion
        and     #$5F            ; -> uppercase
@noconversion: 
				bit			ROM::A2L
				bne			@invset
				ora     #$80
				jsr			ROM::ROM_COUT
				jmp			@next
@invset:
				; we don't need to ora it by 128, as it's already got the highbit set
				U_PHY 		; save y because INVERSE/NORMAL trashes it
				jsr			ROM::ROM_INVERSE
				jsr			ROM::ROM_COUT
				jsr			ROM::ROM_NORMAL
				U_PLY
@next:
        iny
        bne     @loop           ; Branch always
@endstring:
		 		rts		


; print out a Pascal string via COUT
_ROM_LSTR_PRINT:
        sta     ROM::A1L
        stx     ROM::A1H
        ldx     ROM::VERSION
				ldy 		#$00
				lda			(ROM::A1L),y    ; load string length into A3L
				sta			ROM::A3L
        ldy     #$01
@loop:
				lda     (ROM::A1L),y
        cpx     #$06            ; Are we a //e or c?  If not, skip handling converting lowercase
        beq     @noconversion
        cmp     #$60            ; lowercase ?
        bcc     @noconversion
        and     #$5F            ; -> uppercase
@noconversion:
       	ora     #$80						; set hibit and output
        jsr     ROM::ROM_COUT
        iny
				dec 		ROM::A3L
        bne     @loop           ; Branch always
@done:
	 			rts		

; performs mem copy (in same bank), uses ZP A1/A2/A3/A4
; A1 = dst
; A2 = src
; A3 = size
_MEMCOPY:
		; outer loop
		ldy #0
		
		ldx	ROM::A3H
		beq @LREST
		
	@HCOPY:	
		.repeat 2 
		lda	(ROM::A2L), Y
		sta (ROM::A1L), Y
		iny
		.endrepeat
		
		bne @HCOPY
		
		inc ROM::A2H
		inc ROM::A1H
		dex
		bne @HCOPY
		
	@LREST:	
		ldx ROM::A3L
		beq @DONE
		
	@LCOPY:	
		lda (ROM::A2L), Y
		sta (ROM::A1L), Y
		iny
		dex
		bne @LCOPY		
		
	@DONE:	
		RTS

; performs mem fill (in same bank), uses ZP A1/A2/A3
; A1 = dst
; A2 = value (low only)
; A3 = size
_MEMFILL:
		; outer loop
		ldy #0
		
		ldx	ROM::A3H
		beq @LREST
		
	@HCOPY:	
		lda	ROM::A2L
		.repeat 2 
		sta (ROM::A1L), Y
		iny
		.endrepeat
		
		bne @HCOPY
		
		inc ROM::A1H
		dex
		bne @HCOPY
		
	@LREST:	
		ldx ROM::A3L
		beq @DONE
		
	@LCOPY:	
		lda ROM::A2L
		sta (ROM::A1L), Y
		iny
		dex
		bne @LCOPY		
		
	@DONE:	
		RTS
		
		
TEXT_MODE_40:
		STA ROM::SW_80COLOFF
		LDA ROM::SW_TEXT
		LDA ROM::SW_CLRAN3
		LDA ROM::SW_PAGE1
		JSR ROM::ROM_HOME
		RTS

		
GFX_MODE_DHGR:
		LDA ROM::SW_SETAN3
		LDA ROM::SW_HIRES
		LDA ROM::SW_GR
		STA ROM::SW_80COLON
		LDA ROM::SW_FULL
		
		RTS

GFX_MODE_HGR:
		LDA ROM::SW_HIRES
		LDA ROM::SW_GR
		LDA ROM::SW_FULL
		
		RTS

GFX_HGR_PAGE1:
		LDA ROM::SW_PAGE1
		RTS

GFX_HGR_PAGE2:
		LDA ROM::SW_PAGE2
		RTS

GFX_MODE_DHGR_MIXED:
		LDA ROM::SW_SETAN3
		LDA ROM::SW_HIRES
		LDA ROM::SW_GR
		LDA ROM::SW_MIXED
		STA ROM::SW_80COLON
		
		RTS
		
GFX_DHGR_PAGE1:
		STA ROM::SW_80STOREOFF
		LDA ROM::SW_PAGE1
		RTS

GFX_DHGR_PAGE2:
		STA ROM::SW_80STOREOFF
		LDA ROM::SW_PAGE2
		RTS
		
GETKEY:
		LDA ROM::SW_80STOREOFF
        BPL GETKEY
        STA ROM::SW_KEYSTROBE
        RTS

WAIT_VBLANK:
		@vloop1: lda $c019
				bpl @vloop1
		@vloop:	lda $c019
				bmi @vloop ;wait for beginning of VBL interval
		rts
		

	; A1L = incoming
	; A2L = what to mod by
	; returns value in a
MOD8:
		lda ROM::A1L
		sec
	: sbc ROM::A2L
		bcs :-
		adc ROM::A2L
		rts

	; a = random value	
PRNG:
		; test for null
		lda ROM::ARNDL
		bne :+
		lda ROM::ARNDH
		bne :+
		lda #01
		sta ROM::ARNDL
		lda #55
		sta ROM::ARNDH
	:

		LDX   #8
    LDA		ROM::ARNDL
:
    ASL              ; SHIFT THE REG
    ROL   ROM::ARNDH
    BCC   :+
    EOR   #$2D       ; APPLY XOR FEEDBACK WHENEVER A 1 BIT IS SHIFTED OUT
:
  	DEX
    BNE   :--
  	STA   ROM::ARNDL
    CMP   #0         ; RELOAD FLAGS
		rts

	; a = random value
PRNG_2:
    lda		ROM::ARNDH
		lsr
		rol 	ROM::ARNDL
		bcc 	:+
		eor   #$B4
	:
		sta ROM::ARNDH
		eor ROM::ARNDL
		rts

.proc DETECT_APPLE_TYPE
		lda $FBB3
		cmp #$38
		beq origII
		cmp #$EA
		beq plusII_or_III
		cmp #$06
		beq IIc_or_e
		lda #00
		sta machine_type
		rts

	IIc_or_e:
		lda $FBC0
		cmp #$EA
		beq origIIe
		cmp #$E0
		beq enhIIe
		lda #ROM::AT_IIc
		sta machine_type
		rts

	enhIIe:
		lda #ROM::AT_IIeEnh
		sta machine_type
		rts

	origIIe:
		lda #ROM::AT_IIe
		sta machine_type
		rts

	plusII_or_III:
		lda $FB1E
		cmp #$AD
		bne :+
		lda #ROM::AT_IIplus
		sta machine_type
		rts
	:
		lda #ROM::AT_III
		sta machine_type
		rts

	origII:
		lda #ROM::AT_IIorig
		sta machine_type
		rts
.endproc

.proc DETECT_APPLE_MEMORY
		LDA $BF98 							; MACHID in ProDOS
		AND #$30              	; TO CHECK FOR A 128k SYSTEM
		CMP #$30              	; IS IT 128k?
		beq ram128
		CMP #$20
		beq ram64
		lda #$0 ; 48k
		sta machine_ram
		rts
	ram128:
		lda #$2
		sta machine_ram
		rts

	ram64:
		lda #$1
		sta machine_ram
		rts
	
		rts
.endproc

.segment "DATA_H"

machine_type:
	.byte $00

machine_ram:
	.byte $00

.endscope