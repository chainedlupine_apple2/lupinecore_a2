import sys, os, math
import png
from bitstring import *
import argparse

# in order of wiki article palette (also used by the Aseprite A2 palette)
# note: Only grey #1 in here with the same RGB values
# Why?  They show up identically on actual //e hardware
# This does not apply to IIgs but I don't own one so write your own converter to test it!  :^)
# It's easy to add, just add another entry to this table with the correct RGB and pal_hgr_map index
pal_hgr_map_rgb = [
  # this is the "official" colors that you will find in emulators, books, etc. (more RGB style)
  ((0x00, 0x00, 0x00), 0 ), # black
  ((0xdd, 0x00, 0x33), 1 ), # magenta
  ((0x88, 0x55, 0x00), 2 ), # brown
  ((0xff, 0x66, 0x00), 3 ), # orange
  ((0x00, 0x77, 0x22), 4 ), # dark green
  ((0x55, 0x55, 0x55), 5 ), # grey #1
  ((0x11, 0xdd, 0x00), 6 ), # green
  ((0xff, 0xff, 0x00), 7 ), # yellow

  ((0x00, 0x00, 0x99), 8 ), # dark blue
  ((0xdd, 0x22, 0xdd), 9 ), # violet
  ((0xaa, 0xaa, 0xaa), 10), # grey #2
  ((0xff, 0x99, 0x88), 11), # pink
  ((0x22, 0x22, 0xff), 12), # medium blue
  ((0x66, 0xaa, 0xff), 13), # light blue
  ((0x44, 0xff, 0x99), 14), # aqua
  ((0xff, 0xff, 0xff), 15)  # white
]

pal_hgr_map_ntsc = [
  # these are my tweaked colors which match my perceptual tests on real TVs
  ((0x00, 0x00, 0x00), 0 ), # black
  ((0x8d, 0x1c, 0x36), 1 ), # magenta
  ((0x57, 0x35, 0x00), 2 ), # brown
  ((0xff, 0x66, 0x00), 3 ), # orange
  ((0x00, 0x57, 0x19), 4 ), # dark green
  ((0x55, 0x55, 0x55), 5 ), # grey #1
  ((0x0e, 0xad, 0x00), 6 ), # green
  ((0xa8, 0xa8, 0x00), 7 ), # yellow

  ((0x00, 0x00, 0x99), 8 ), # dark blue
  ((0x8c, 0x38, 0xe0), 9 ), # violet
  ((0x4a, 0x4a, 0x4a), 10), # grey #2
  ((0xff, 0x9e, 0xe0), 11), # pink
  ((0x1f, 0x9a, 0xff), 12), # medium blue
  ((0xae, 0xab, 0xff), 13), # light blue
  ((0x44, 0xff, 0x99), 14), # aqua
  ((0xff, 0xff, 0xff), 15)  # white
]

# use the NTSC map
pal_hgr_map = pal_hgr_map_ntsc

# in order of bit pattern
dhgr_bit_map = [
  0b0000, # 0,black
  0b0001, # 1,magenta (aka red in some references)
  0b0010, # 2,brown
  0b0011, # 3,orange
  0b0100, # 4,dark green
  0b0101, # 5,grey #1
  0b0110, # 6,green
  0b0111, # 7,yellow
  0b1000, # 8,dark blue
  0b1001, # 9,violet
  0b1010, # A,grey #2
  0b1011, # B,pink
  0b1100, # C,medium blue
  0b1101, # D,light blue
  0b1110, # E,aqua
  0b1111, # F,white
]

# note: reversed, ie: 01 is 10
# take the hgr colors (0, 3, 6, 9, 12, 15), divide by 3 and it will index this array
hgr_bit_map = [
  0b00, # 0,black
  0b10, # 1,orange (group 1)
  0b10, # 2,green (group 0)
  0b01, # 3,violet (group 0)
  0b01, # 4,light blue (group 1)
  0b11, # 5,white
]

hgr_colors = [0, 3, 6, 9, 12, 15]
hgr_neutral_colors = [0, 15]    # black, white
hgr_group0_colors = [6, 9]      # green, violet
hgr_group1_colors = [3, 12]     # orange, light blue
hgr_fringe_colors = [3, 6, 9, 12]       # green, light blue
hgr_group_colors = [hgr_group0_colors, hgr_group1_colors]

process_width = 140
process_height = 192

debug_mode = False

def map_rgb(r, g, b, color_map):
  for pal_idx in color_map:
    if r == pal_idx[0][0] and g == pal_idx[0][1] and b == pal_idx[0][2]:
      return pal_idx[1]
  return None


def process_hgr(pngfilenm, hgrfilename):
  global process_width, process_height, pal_hgr_map

  def find_group(gpixels):
    #print (gpixels)
    for x in range(0, len(gpixels)):
      if gpixels[x] not in hgr_neutral_colors:
        found_group = 0 if gpixels[x] in hgr_group0_colors else 1
        #print("pixel {} is group {}".format(gpixels[x], found_group))
        return found_group
    return -1

  # return None if no violations, othewise gpixel index of violation
  def detect_group_violation(gpixels, group_num):
    for x in range(0, len(gpixels)):
      if gpixels[x] not in hgr_neutral_colors:
        if group_num != -1 and gpixels[x] not in hgr_group_colors[group_num]:
          return x

    return None

  print ("Reading " + pngfilenm)

  error_filenm = os.path.splitext(pngfilenm)[0] + "_error.png"

  pngfile = open(pngfilenm, "rb")
  pngstream = png.Reader(pngfile)

  img_data = pngstream.asRGBA8()
  is8bit = img_data[3]['bitdepth'] == 8
  img_rows = list(img_data[2])
  img_width = img_data[0]
  img_height = img_data[1]

  if img_width < process_width and img_height < process_height:
    raise Exception("Size is not {}x{}!".format(process_width, process_height))

  # will contain png palette idx and HGR bit pattern
  print("Convert image to HGR palette as {}x{}...".format(process_width, process_height))
  pal_rows = []
  for y in range(0, process_height):
    col_data = img_rows[y]
    pal_col = [None] * (len(col_data) // 4)

    for x in range (0, len(col_data), 4) :
      r = col_data[x]
      g = col_data[x+1]
      b = col_data[x+2]
      result = map_rgb(r, g, b, pal_hgr_map)
      if result is None:
        raise Exception("Cannot find color ${:02X},${:02X},${:02X} in palette at pixel {},{}!".format(r, g, b, x, y))
      pal_col[x // 4] = result

    pal_rows.append(pal_col)

  # now convert to our bytearrays
  print("Generating Apple II memory buffer...")
  hgr_buffer = bytearray (0x2000)   # 8192 bytes

  error_rows = []
  error_count = 0
  gpixels = []
  testpixels = []

  for y in range(0, process_height):
    addr = (int(y/64) | int(0)) * 0x28 + (int(y%8) | int(0)) * 0x400 + (int(y/8) & int(7)) * 0x80
    #print ("addr={:04x}".format(addr))
    row_data = pal_rows[y]

    error_col = []

    for xp in range(0, process_width * 3):
      error_col.append(0)

    #print(row_data)

    prior_idx = 0

    # detect fringe colors
    for px in range(0, process_width):
      fringe_idx = row_data[px]
      if prior_idx != fringe_idx:
        fringe = False
        if fringe_idx in hgr_fringe_colors and prior_idx in hgr_fringe_colors:
          fringe = True
        #print("x={} curr={} prior={} fringe={}".format(px, fringe_idx, prior_idx, fringe))
        if fringe:
          error_col[((px) * 3) + 1] = 255
          error_count += 1
      prior_idx = fringe_idx

    # rules: pixels are processed in groups of 7, where each 7 pixels is represented by 2 bytes (3.5 pixels per byte)
    # bytes in a scanline go from left to right, but the bits within that byte are processed by the A2 from right to left
    # each byte uses #7 bit to select two color modes: 0=green/violet, 1=orange/light blue
    # the pixel that is split between the two bytes can ONLY be 00 (black) or 11 (white) otherwise color clash occurs
    # due to the NTSC encoding, only group 0 pixels can display on odd horiz coords, and group 1 on even coords, otherwise clash occurs
    for x in range(0, process_width, 7):
      mem_off = addr + ((x // 7) * 2)

      # first, check to see if use green/violet and orange/light blue in the same byte (first 3 pixels, and last 3 pixels)
      first_group = -1
      second_group = -1

      # first 3 pixels
      gpixels.clear()
      gpixels.append (row_data[x + 0])
      gpixels.append (row_data[x + 1])
      gpixels.append (row_data[x + 2])
      first_group = find_group(gpixels)
      bad_pixel = detect_group_violation(gpixels, first_group)
      if bad_pixel is not None:
        if debug_mode:
          print("left pixel #{} (idx={}) violating group {}".format(bad_pixel, row_data[x + bad_pixel], first_group))
        error_col[((x + bad_pixel) * 3) + 2] = 255
        error_count += 1

      # last 3 pixels
      gpixels.clear()
      gpixels.append (row_data[x + 4])
      gpixels.append (row_data[x + 5])
      gpixels.append (row_data[x + 6])
      second_group = find_group(gpixels)
      bad_pixel = detect_group_violation(gpixels, second_group)
      if bad_pixel is not None:
        if debug_mode:
          print("right pixel #{} (idx={}) violating group {}".format(bad_pixel, row_data[x + bad_pixel], second_group))
        error_col[((x + 4 + bad_pixel) * 3) + 2] = 255
        error_count += 1

      #print("detected groups: left={}, right={}".format(first_group, second_group))

      # middle pixel
      middle_idx = row_data[x + 3]

      if middle_idx in hgr_group0_colors and first_group == -1:
        first_group = 0

      if middle_idx in hgr_group1_colors and first_group == -1:
        first_group = 1

      # doesn't matter for both, so let's make it match the group that we used to the left of this pixel (if any)
      if (first_group == -1 and second_group == -1):
        if x >= 7:
          prior_group = (hgr_buffer[mem_off - 2] & 0x80) >> 7
          first_group = second_group = prior_group
        else:
          first_group = 0
          second_group = 0

      # make right group match left group if it doesn't matter
      if (first_group >= 0 and second_group == -1):
        second_group = first_group

      # make left group match right group if it doesn't matter
      if (first_group == -1 and second_group >= 0):
        first_group = second_group

      #print("final groups: left={}, right={}".format(first_group, second_group))

      # second, check if pixel #4 is anything but black and white, because it cannot be split
      if middle_idx not in hgr_neutral_colors:
        # not a neutral color, so we must do the check
        if first_group != second_group:
          #print (first_group, second_group, middle_idx)
          error_col[((x + 3) * 3) + 0] = 255
          error_count += 1

      # now, generate the bytes
      first_byte = 0
      second_byte = 0

      for po in range(0, 3):
        bitvalue_left = hgr_bit_map[row_data[x + po] // 3]
        bitvalue_left = bitvalue_left << (po * 2)
        first_byte = first_byte | bitvalue_left

        bitvalue_right = hgr_bit_map[row_data[x + 4 + po] // 3]
        bitvalue_right = bitvalue_right << (po * 2 + 1)
        second_byte = second_byte | bitvalue_right

      # now split the middle pixel up
      bitvalue_middle = hgr_bit_map[middle_idx // 3]
      first_byte = first_byte | ((bitvalue_middle & 1) << 6)  # mask off bit 0, shift to 6th position
      second_byte = second_byte | ((bitvalue_middle >> 1) & 1)  # mask off bit 2, right shift over and then place in 0th position

      # set groups
      first_byte = first_byte | (first_group << 7)
      second_byte = second_byte | (second_group << 7)

      if debug_mode:
        print ("{:08b} {:08b}".format(first_byte, second_byte))

      # now write the two bytes
      hgr_buffer[mem_off] = first_byte
      hgr_buffer[mem_off+1] = second_byte

    error_rows.append (error_col)


  errorpng = png.from_array(error_rows, 'RGB', {"height": process_height, "width": process_width})
  errorpng.save(error_filenm)

  print("Number of error pixels: {} out of {}".format(error_count, process_width * process_height))

  out_file = open (hgrfilename, 'bw')
  out_file.write(hgr_buffer)
  out_file.close()


def process_dhgr(pngfilenm, dhgrfilename):
  global pal_hgr_map
  #lines_to_process = 120 if sys.argv[2] == "mixed" else 192
  lines_to_process = 192


  print ("Reading " + pngfilenm)

  pngfile = open(pngfilenm, "rb")

  pngstream = png.Reader(pngfile)
  img_data = pngstream.asRGBA8()
  is8bit = img_data[3]['bitdepth'] == 8
  img_rows = list(img_data[2])
  img_width = img_data[0]
  img_height = img_data[1]

  if img_width < process_width and img_height < process_height:
    raise Exception("Size is not {}x{}!".format(process_width, process_height))

  # will contain png palette idx and DHGR bit pattern
  print("Convert image to DHGR palette as {}x{}...".format(process_width, process_height))
  pal_rows = []
  for y in range(0, process_height):
    col_data = img_rows[y]
    pal_col = [None] * (len(col_data) // 4)

    for x in range (0, len(col_data), 4) :
      r = col_data[x]
      g = col_data[x+1]
      b = col_data[x+2]
      result = map_rgb(r, g, b, pal_hgr_map)
      if result is None:
        raise Exception("Cannot find color ${:02X},${:02X},${:02X} in palette at pixel {},{}!".format(r, g, b, x, y))
      pal_col[x // 4] = result

    pal_rows.append(pal_col)

  # now convert to our bytearrays
  print("Generating aux and main buffer...")
  aux_buffer = bytearray (0x2000)   # 8192 bytes
  main_buffer = bytearray (0x2000)   # 8192 bytes

  bit_patterns = [None] * 7

  test = 1

  #for y in range(0, 1): #192):
  for y in range(0, lines_to_process):
    addr = (int(y/64) | int(0)) * 0x28 + (int(y%8) | int(0)) * 0x400 + (int(y/8) & int(7)) * 0x80
    #print ("addr={:04x}".format(addr))
    row_data = pal_rows[y]
    # now process a line

    #for x in range(0, 7, 7): #140, 7):
    for x in range(0, 140, 7):
      # time to get LAAAAAAAAAAAAZZZZZZZZZZZZZZZZZZZZYYYYYYYYYYYYYYYYY with bitstring

      #print ("pixel ", x,  " of row ", y)
      # lookup bit pattern
      #dhgr_bit_map
      bit_patterns[0] = dhgr_bit_map[row_data[x]]
      bit_patterns[1] = dhgr_bit_map[row_data[x+1]]
      bit_patterns[2] = dhgr_bit_map[row_data[x+2]]
      bit_patterns[3] = dhgr_bit_map[row_data[x+3]]
      bit_patterns[4] = dhgr_bit_map[row_data[x+4]]
      bit_patterns[5] = dhgr_bit_map[row_data[x+5]]
      bit_patterns[6] = dhgr_bit_map[row_data[x+6]]

      ba = BitArray()

      for p_idx in range(0, len(bit_patterns)):
        bp = bit_patterns[p_idx]
        bits = BitArray(uint=bp, length=4)
        ba.append (bits)

      # reverse each 7 bit pairs
      ba.reverse(0,7)
      ba.reverse(7,14)
      ba.reverse(14,21)
      ba.reverse(21,28)

      # insert padding every 7 bits
      ba.insert('uint:1=0', 0)
      ba.insert('uint:1=0', 8)
      ba.insert('uint:1=0', 16)
      ba.insert('uint:1=0', 24)

      byte_idx = math.floor(x / 7) * 2
      #print (byte_idx)

      data_bytes = ba.bytes
      aux_buffer[addr + byte_idx] = ba.bytes[0]
      main_buffer[addr + byte_idx] = ba.bytes[1]
      aux_buffer[addr + byte_idx + 1] = ba.bytes[2]
      main_buffer[addr + byte_idx + 1] = ba.bytes[3]

  # write aux and main buffers
  out_file = open (dhgrfilename, 'bw')
  out_file.write(aux_buffer)
  out_file.write(main_buffer)

  out_file.close()

  print ("Done!")

  pngfile.close()

def main():
  global process_width, process_height, debug_mode

  parser = argparse.ArgumentParser(description="Convert PNGs to Apple II HGR/DHGR")
  parser.add_argument('--png', required=True)
  parser.add_argument('--out')
  parser.add_argument('--debug', action='store_true')
  parser.add_argument('--width', default=140, type=int)
  parser.add_argument('--height', default=192, type=int)
  parser.add_argument('--color', choices=['ntsc', 'rgb'], default='ntsc')
  parser.add_argument('--format', choices=['dhgr', 'hgr'], default='hgr')
  args = parser.parse_args()

  #print(args)

  process_width = args.width
  process_height = args.height
  debug_mode = args.debug

  if args.out is None:
    outfilename = os.path.splitext(args.png)[0]
  else:
    outfilename = args.out

  pal_hgr_map = pal_hgr_map_ntsc if args.color == "ntsc" else pal_hgr_map_rgb

  print ("Using color map {}.".format(args.color))

  if args.format == "hgr":
    process_hgr (args.png, outfilename + ".hgr")

  if args.format == "dhgr":
    process_dhgr (args.png, outfilename + ".dhgr")

  sys.exit()



if __name__ == "__main__":
  main()
