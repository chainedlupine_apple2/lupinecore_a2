# kompressor6502 by ChainedLupine (David Grace) (c) 2019

# byte-pair encoding (aka DTE or digram coding) compressor for the 6502 processor

# You do not use a Macintosh
# Instead you use a Tandy
# KOMPRESSOR break your glowstick
# KOMPRESSOR eat your candy
# KOMPRESSOR open jars
# KOMPRESSOR release ants
# KOMPRESSOR watch you scream because
# KOMPRESSOR DOES NOT DANCE

import sys, os
from bpe import *
from filetypes import *

"""
kompressor accepts several types of files:  
  * standard, singular binary file.
  * DHGR Apple2 graphics format
  * text file that is a ca65-style assembler listing.  

---- Single Binary File ----
  
  In the first case, it just writes the encoded version out (w/ .kr extension) and that's that.

  File extension: .kr

  File format:  
    bytes 0-1:        magic_bytes_bin
    bytes 2-3:        little-endian encoded data size
    bytes 4-5:        little-endian original data size
    bytes 6-133:      128 bytes of left-part of encoding pairs table
    bytes 134-261:    128 bytes of right-part of encoding pairs table
    bytes 262-(rest): actual encoded data

-- DHGR Apple2 graphics format ---

  These files are just two 8192 dumps of MAIN and AUX RAM of the A2 graphics memory Double HiRes Graphics memory.  So, they are compressed as two seperate blocks.

  File extension: .dhkr

  File format:
    bytes 0-1:        magic_bytes_dhgr
    bytes 2-3:        encoded size of AUX chunk
    bytes 4-5:        encoded size of MAIN chunk
    bytes 6-133:      128 bytes of left-part of encoding pairs table
    bytes 134-261:    128 bytes of right-part of encoding pairs table
    bytes 262-(rest): actual encoded data

---- ca65 Assembler Source ----

  CA65 assembler source will then take all the data it finds in the given file and generate an encoded binary data version, along with a ca65 include file.
  This include will contain a list of the same labels used in the source file but with 16-bit word indexes into the compressed data, so you can decode on the
  fly based on the index.  Each string is stored as zero-terminated, so you can just decode each string up until a $00 byte.  There is no limit on how long
  a single string can be.

  Supported data: strings (will be zero-terminated automatically)

  Example:

    My_Label:
      "This is a string"

    File extension: .krt (.inc for the label definitions)

  Binary file format:
    bytes 0-1:        magic_bytes_txt
    bytes 2-129:      128 bytes of left-part of encoding pairs table
    bytes 130-257:    128 bytes of right-part of encoding pairs table
    bytes 258-(rest): actual encoded data

"""


sourcefn = sys.argv[1]
ext = os.path.splitext(sourcefn)[1]
if ext.lower() == '.dhgr':
  FiletypeDHGRProcessor.process(sourcefn)
if ext.lower() == '.txt':
  FiletypeTextProcessor.process(sourcefn)
else:
  FiletypeFlatBinProcessor.process(sourcefn)