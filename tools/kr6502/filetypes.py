import sys, os
from bpe import *

magic_bytes_bin = [0x43, 0x4C]
magic_bytes_dhgr = [0x44, 0x52]
magic_bytes_txt = [0x44, 0x54]


class FiletypeFlatBinProcessor:

  @staticmethod
  def process(sourcefn):
    destfn = os.path.splitext(sourcefn)[0] + '.kr'
    testfn = os.path.splitext(sourcefn)[0] + '.decoded'

    inputbuffer = []

    with open(sourcefn, "rb") as inputf:
      inputbuffer = inputf.read()

    # data is a 2d matrix -- number of chunks, and then a group of bytes per chunk
    # note that pure binary data is just one chunk
    # we split data like this because we might want to have multi-indexed data (ie: series of lines of text or smaller binary blobs)
    chunks = [None] * 1
    chunks[0] = { "label": "data", "bytes" : list(inputbuffer) }

    compressor = BytePairEncoder()
    coded_pairs_table = list()

    try:
      # first, verify that our data is safe to proceed with
      for i in range(0, len(chunks)):
        print("analyzing chunk {}".format(i))
        data = chunks[i]["bytes"]
        compressor.analyze(data)

      compressor.encode_chunks(chunks)

      compressor.decode_chunks(chunks)

      #print("bpe_left len={}, bpe_right len={}".format(len(compressor.bpe_left), len(compressor.bpe_right)))

      coded_pairs_table = compressor.get_code_table()

      for i in range(0, len(chunks)):
        data = chunks[i]["bytes"]
        encoded = chunks[i]["encoded"]
        decoded = chunks[i]["decoded"]
        encoded_size = len(encoded)
        table_size = len(coded_pairs_table)
        print("chunk #{} bytes size={}, encoded size={} table size={} total={}, decoded size={}".format(i, len(data), encoded_size, table_size, encoded_size + table_size + 6, len(decoded)))
        if data != decoded:
          print("Error with decode: chunk #{} does not decodes back into same original data".format(i))

      print ("Creating {}...".format(destfn))
      with open(destfn, "wb") as outputf:
        outputf.write(bytes(magic_bytes_bin))
        encoded_size_bytes=len(chunks[0]["encoded"]).to_bytes(2, byteorder='little')
        outputf.write(encoded_size_bytes)
        decoded_size_bytes=len(chunks[0]["bytes"]).to_bytes(2, byteorder='little')
        outputf.write(decoded_size_bytes)
        outputf.write(bytes(coded_pairs_table))
        outputf.write(bytes(chunks[0]["encoded"]))

      #with open(testfn, "wb") as outputf:
      #  outputf.write(bytes(chunks[0]["decoded"]))

    except AnalyzeError as err:
      print("Failure analyzing source: {}".format(err.message))

class FiletypeDHGRProcessor:
  
  @staticmethod
  def process(sourcefn):
    print("Processing as DHGR...")
    destfn = os.path.splitext(sourcefn)[0] + '.dhkr'
    testfn = os.path.splitext(sourcefn)[0] + '.decoded'

    inputbuffer = []

    with open(sourcefn, "rb") as inputf:
      inputbuffer = inputf.read()

    if len(inputbuffer) != 16384:
      raise Exception("File is not 16384 bytes, you sure this is a DHGR?")

    # data is a 2d matrix -- number of chunks, and then a group of bytes per chunk
    # note that pure binary data is just one chunk
    # we split data like this because we might want to have multi-indexed data (ie: series of lines of text or smaller binary blobs)
    chunks = [None] * 2
    chunks[0] = { "label": "bank_aux", "bytes" : list(inputbuffer[0:8192]) }
    chunks[1] = { "label": "bank_main", "bytes" : list(inputbuffer[8192:]) }

    compressor = BytePairEncoder()
    coded_pairs_table = list()

    try:
      # first, verify that our data is safe to proceed with and build bpe tables
      for i in range(0, len(chunks)):
        print("analyzing chunk {}".format(i))
        data = chunks[i]["bytes"]
        compressor.analyze(data)

      compressor.encode_chunks(chunks)

      compressor.decode_chunks(chunks)

      #print("bpe_left len={}, bpe_right len={}".format(len(compressor.bpe_left), len(compressor.bpe_right)))

      coded_pairs_table = compressor.get_code_table()

      total_encoded_size = 0
      for i in range(0, len(chunks)):
        data = chunks[i]["bytes"]
        encoded = chunks[i]["encoded"]
        decoded = chunks[i]["decoded"]
        encoded_size = len(encoded)
        total_encoded_size += encoded_size
        print("chunk #{} bytes size={}, encoded size={}, decoded size={}".format(i, len(data), encoded_size, len(decoded)))
        if data != decoded:
          print("Error with decode: chunk #{} does not decodes back into same original data".format(i))

      table_size = len(coded_pairs_table)
      total_size = total_encoded_size + table_size + 4
      print("total compressed size={}".format(total_size))

      if total_encoded_size > 8192:
        print ("  ********** WARNING:  Greater than 8192 bytes encoded! **********")

      print ("Creating {}...".format(destfn))
      with open(destfn, "wb") as outputf:
        outputf.write(bytes(magic_bytes_dhgr))
        chunk0_size = len(chunks[0]["encoded"])
        chunk0_size_bytes=chunk0_size.to_bytes(2, byteorder='little')
        outputf.write(chunk0_size_bytes)
        chunk1_size = len(chunks[1]["encoded"])
        chunk1_size_bytes=chunk1_size.to_bytes(2, byteorder='little')
        outputf.write(chunk1_size_bytes)
        outputf.write(bytes(coded_pairs_table))
        # now write the two chunks
        for i in range(0, len(chunks)):
          outputf.write(bytes(chunks[i]["encoded"]))

      #with open(testfn, "wb") as outputf:
      #  outputf.write(bytes(chunks[0]["decoded"]))

    except AnalyzeError as err:
      print("Failure analyzing source: {}".format(err.message))

class FiletypeTextProcessor:
  
  @staticmethod
  def process(sourcefn):

    def append_chunk(chunks, curr_label, curr_text):
      str = "^".join(curr_text)
      data = [ord(c) for c in str]
      data.append(0)
      new_chunk = { "label": curr_label, "bytes" : data}
      return new_chunk

    print("Processing as Text...")
    destfn = os.path.splitext(sourcefn)[0] + '.krt'
    destlabelfn = os.path.splitext(sourcefn)[0] + '_def.inc'
    testfn = os.path.splitext(sourcefn)[0] + '.decoded'

    inputbuffer = []

    with open(sourcefn, "r") as inputf:
      inputlines = filter(None, inputf.read().splitlines())

    chunks = []
    curr_text = []
    curr_label = None

    for line in inputlines:
      if line.startswith(";"):
        # ignore
        pass
      elif line.endswith(":"):
        if curr_label:
          # take the text we have, and output it as a new chunk
          new_chunk = append_chunk(chunks, curr_label, curr_text)
          chunks.append(new_chunk)
          #print("new_chunk=", new_chunk)

        curr_text = []
        curr_label = line[:-1]
      else:
        if len(line.strip()) is not 0:
          curr_text.append (line.strip().strip("\"").strip("\'"))
          if len(curr_text) > 1024:
            print("** Warning, line for {} exceeds 1024 bytes!".format(curr_label))

    if curr_label is not None:
      new_chunk = append_chunk(chunks, curr_label, curr_text)
      chunks.append(new_chunk)
      #print("final_chunk=", new_chunk)

    # data is a 2d matrix -- number of chunks, and then a group of bytes per chunk
    # note that pure binary data is just one chunk
    # we split data like this because we might want to have multi-indexed data (ie: series of lines of text or smaller binary blobs)
    
    compressor = BytePairEncoder()
    coded_pairs_table = list()

    try:
      # first, verify that our data is safe to proceed with and build bpe tables
      for i in range(0, len(chunks)):
        #print("analyzing chunk {}".format(i))
        data = chunks[i]["bytes"]
        compressor.analyze(data)

      compressor.encode_chunks(chunks)

      compressor.decode_chunks(chunks)

      #print("bpe_left len={}, bpe_right len={}".format(len(compressor.bpe_left), len(compressor.bpe_right)))

      coded_pairs_table = compressor.get_code_table()

      # build offsets
      offset = 2 + 256
      for i in range(0, len(chunks)):
        chunks[i]["offset"] = offset
        offset += len(chunks[i]["encoded"])

      total_encoded_size = 0
      for i in range(0, len(chunks)):
        data = chunks[i]["bytes"]
        encoded = chunks[i]["encoded"]
        decoded = chunks[i]["decoded"]
        #print(chunks[i]["label"])
        #print(decoded)
        encoded_size = len(encoded)
        total_encoded_size += encoded_size
        print("chunk #{} bytes size={}, offset={} encoded size={}, decoded size={}, label={}".format(i, len(data), chunks[i]["offset"], encoded_size, len(decoded), chunks[i]["label"]))
        if data != decoded:
          print("Error with decode: chunk #{} does not decodes back into same original data".format(i))

      table_size = len(coded_pairs_table)
      total_size = total_encoded_size + table_size + 2
      print("total compressed size={}".format(total_size))

      if total_encoded_size > 4864:
        print ("  ********** WARNING:  Greater than 4864 bytes encoded! **********")


      print ("Creating {}...".format(destfn))
      with open(destfn, "wb") as outputf:
        outputf.write(bytes(magic_bytes_txt))
        outputf.write(bytes(coded_pairs_table))
        # now write the two chunks
        for i in range(0, len(chunks)):
          outputf.write(bytes(chunks[i]["encoded"]))

      print ("Creating {}...".format(destlabelfn))
      with open(destlabelfn, "w") as textf:
        textf.write (";----------------------------------------------\n")
        textf.write ("; source: {}\n".format(sourcefn))
        textf.write ("; Auto-generated by kompressor.py\n")
        textf.write (";----------------------------------------------\n")
        textf.write ("\n")
        for i in range(0, len(chunks)):
          textf.write("{}:\n".format(chunks[i]["label"]))
          textf.write("\t.addr ${:04x}\n\n".format(chunks[i]["offset"]))

    except AnalyzeError as err:
      print("Failure analyzing source: {}".format(err.message))
