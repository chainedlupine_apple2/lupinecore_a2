"""
  En/decompressor class for byte-pair encoding, using this algorithm: http://www.drdobbs.com/a-new-algorithm-for-data-compression/184402829?pgno=3

  Recursively swaps byte pairs with unused bytes in byte stream, with an unused byte that represents those two bytes.

  Uses one global pair swap table, but recursively utilizes it.

  Restrictions:  The incoming data must have 
"""

class AnalyzeError(Exception):
    def __init__(self, message):
        self.message = message

class BytePairEncoder:

  max_depth = 16

  def __init__(self):
    self.bpe_left = []
    self.bpe_right = []

    self.highr = 255
    self.lowr = 128

    self.current_depth = 0

    # 256x256 values
    self.freqs = [[0] * 256 for i in range(256)]

  def analyze(self, data):
    used = [False] * 256

    max_val = 0
    min_val = 255

    for j in range(0, len(data)):
      if data[j] > max_val:
        max_val = data[j]

      if data[j] < min_val:
        min_val = data[j]

      used[data[j]] = True

    used_amount = 0
    for i in range(0, 256):
      if used[i]:
        used_amount += 1

    max_gapsize = 0
    gap_start = 0
    for i in range(0, 256):
      if used[i] == False:
        j = i + 1
        while j < 256 and used[j] == False:
          j += 1
        
        if j-i > max_gapsize:
          gap_start = i
          max_gapsize = j-i

        i = j

    #print("gap_start={} max_gapsize={}".format(gap_start, max_gapsize))
    hr = gap_start + max_gapsize - 1
    lr = gap_start
    #print("hr={} lr={}".format(hr, lr))

    if max_gapsize < 4:
      raise AnalyzeError("Max gapsize is less than 4.")

    if gap_start > 128:
      raise AnalyzeError("gap start is greater than 128.  gap_start={}, max_gapsize={}".format(gap_start, max_gapsize))



  def encode_chunks(self, chunks):

    # build 256*256 pair frequencies for each byte stream in each chunk
    for i in range(0, len(chunks)):
      data = chunks[i]["bytes"]
      for j in range(0, len(data)-1):
        self.freqs[data[j]][data[j+1]] += 1
      # also clone data
      chunks[i]["encoded"] = list(data)

    encoded = list(data)

    self.current_depth = 0

    # bulk of encoding happens here
    while True:
      maxfreq = 0
      maxi = -1
      maxj = -1

      for i in range(0, 256):
        for j in range(0, 256):
          if self.freqs[i][j] > maxfreq:
            maxfreq = self.freqs[i][j]
            maxi = i
            maxj = j
            #print("highest freq={} in {},{}".format(maxfreq, maxi, maxj))

      if maxfreq < 3:
        #print("stop: maxfreq < 3")
        break

      #print ("maxfreq={} maxi={} maxj={}".format(maxfreq, maxi, maxj))

      self.bpe_left.append(maxi)
      self.bpe_right.append(maxj)

      # now do byteswap
      for i in range(0, len(chunks)):
        data = chunks[i]["bytes"]
        encoded = chunks[i]["encoded"]
        #print ("byteswap on chunk #{}, src len={}, encoded len={}".format(i, len(data), len(encoded)))

        for j, val in enumerate(encoded):
          if j >= len(encoded) - 1:
            break
          if encoded[j] == maxi and encoded[j+1] == maxj:
            #print("  swap found at {} for freq matrix {} {} ".format(j, maxi, maxj))
            # assign a new code for this high-frequency pair
            code = len(self.bpe_left) + self.lowr - 1
            if j != 0:
              previous = encoded[j-1]
              self.freqs[previous][maxi] -= 1
              self.freqs[previous][code] += 1

            # now remove the old pair and replace it with our new coding
            self.freqs[maxi][maxj] -= 1
            encoded.pop(j+1)
            encoded[j] = code

            if j < len(encoded) - 1:
              next = encoded[j+1]
              self.freqs[maxj][next] -= 1
              self.freqs[code][next] += 1
              self.current_depth += 1

        chunks[i]["encoded"] = encoded

      #print("bpe_left len={}, bpe_right={}".format(len(self.bpe_left), len(self.bpe_right)))

      # stop if we've exceeded the length of our left pair
      if len(self.bpe_left) > self.highr - self.lowr:
        #print("stop: bpe_left len={}".format(len(self.bpe_left)))
        break


  def resolve_code(self, code, depth):
    out = list()
    
    if depth > self.current_depth:
      self.current_depth = depth

    if (code >= self.lowr and code <= self.highr):
      #print("{} ={} code={:02X} idx={:02X} L={:02X} R={:02X}".format (depth * " ", depth, code, code - self.lowr, self.bpe_left[code - self.lowr], self.bpe_right[code - self.lowr]))
      leftcodes = self.resolve_code(self.bpe_left[code - self.lowr], depth + 1)
      rightcodes = self.resolve_code(self.bpe_right[code - self.lowr], depth + 1)
      out.extend(leftcodes)
      out.extend(rightcodes)
    else:
      out.append(code)

    return out


  def decode(self, encoded):
    self.current_depth = 0
    decoded=list()
    for j in range(0, len(encoded)):
      codes = self.resolve_code(encoded[j], 0)
      #print(", ".join("${:02x}".format(num) for num in codes))
      #sys.exit()
      decoded.extend(codes)
    return decoded

  def decode_chunks(self, chunks):
    for i in range(0, len(chunks)):
      encoded = chunks[i]["encoded"]
      decoded = self.decode(encoded)
      chunks[i]["decoded"] = decoded      

  def get_code_table(self):
    table = list()
    # pad to 128 bytes
    outl = list(self.bpe_left)
    outl += [0] * (128 - len(outl))
    table.extend(outl)
    outr = list(self.bpe_right)
    outr += [0] * (128 - len(outr))
    table.extend(outr)
    return table

