; bootspot is loaded at $2000 (as system file)
; Copies itself to $300 and runs from there
; database is 5 entries max :  LSTR of filename (15+1 bytes * 5)
; data on low side is loaded at $200
; io_buffer is at $0C00
; sets prefix if it detects BASIC.SYSTEM

; * Loop through entries, last to first
;   * Open filename
;   * get file info to get aux addr
;   * Read entire file to aux addr
; * Jump to first entry aux addr

.include "utils.inc"

.include "prodos-defs.inc"
.include "prodos-snippets.inc"
.include "apple2.inc"
.include "apple2rom-defs.inc"

.import __CODE_L_LOAD__, __CODE_L_RUN__, __CODE_L_SIZE__
.import __DATA_L_LOAD__, __DATA_L_RUN__, __DATA_L_SIZE__

PTRL            := $EB
PTRH            := $EC
TEMP            := $ED
PATHNAME        := $0280    ; storage for pathname
BASIC_FLAG      := $02FF    ; set 1 if basic loaded
IO_BUFFER_ADDR  := $0C00    ;

MODULE_ENTRIES  = 5

; -----------------------------------------------------------------------------------------------
;                                      HIGH SIDE CODE
; -----------------------------------------------------------------------------------------------

.segment "CODE_H"
    ; code that runs on start of system file

STARTUP:
    ; Reset stack
    ldx   #$FF
    txs

    ; set text mode
    sta   SW_TEXT
    sta   SW_80COLOFF
    sta   SW_ALTCHAROFF
    sta   SW_PAGE1
    jsr   ROM_HOME

    ; now verify ProDOS is loaded
    jsr   DETECT_PRODOS
    bcs   :+
    jmp   NO_PRODOS_ERROR
:

    ; move cursor to center and print loading prompt
    lda   #15
    sta   ROM_CH
    lda   #11
    sta   ROM_CV
    jsr   ROM_CURDOWN
    jsr   ROM_CURUP

    lda   #<LOADING_STR
    ldx   #>LOADING_STR
    jsr   PRINT_H

    jsr   ROM_CROUT
    jsr   ROM_CROUT

    jsr   CLEAR_PATHNAME

    ; now verify that we have a prefix set
    lda   PREFIXSET
    beq   :+
      jmp skip_reset
  :

    ; no prefix, we need to set it
    ; first look for BASIC.SYSTEM
    jsr   DETECT_BASIC_SYSTEM
    bcc   no_basic

    lda   #1
    sta   BASIC_FLAG

    ; we are inside the BASIC.SYSTEM sys, so we need to do a bit extra logic to figure out the current disk
    lda   BS_LASTSLOT
    asl
    asl
    asl
    asl   ; convert 0X to X0 for slot number
    sta   TEMP
    lda   BS_LASTDRIVE
    sec   
    sbc   #1    ; convert 1-index to 0-index
    clc
    ror
    bcc   :+
    ora   #$80
  :
    ora   TEMP  ; combine with shifted slot number
    sta   ON_LINE_UNIT_NUM
    jmp   get_volname

  no_basic:
    ; we will just use the last device accessed by ProDOS for our source
    lda   #0
    sta   BASIC_FLAG

    lda   LASTDEVICE
    sta   ON_LINE_UNIT_NUM

  get_volname:
    jsr   MLI
    .byte CALL_ON_LINE
    .addr ON_LINE_PARAM
    ; mask out the slot/drive nibble
    nop
    nop
    nop
    nop
    nop
    lda   PATHNAME+1
    and   #$0F      ; mask off the high nibble just leaving volume length
    clc
    adc   #1        ; add one
    sta   PATHNAME
    lda   #$2F      ; '/'
    sta   PATHNAME+1

    ; now set the prefix to this
    jsr   MLI
    .byte CALL_SET_PREFIX
    .addr SET_PREFIX_PARAM

  skip_reset:
    ; must relocate AFTER we do the MLI calls
    ; Relocate CODE_L to $300 
    ldx   #<(__CODE_L_SIZE__)
:   lda   __CODE_L_LOAD__-1,x
    sta   __CODE_L_RUN__-1,x
    dex
    bne   :-

    ; Relocate DATA_L to $200
    ldx   #<(__DATA_L_SIZE__)
:   lda   __DATA_L_LOAD__-1,x
    sta   __DATA_L_RUN__-1,x
    dex
    bne   :-

    ; We've copied everything, so jump to low-memory loader
    jmp     __CODE_L_RUN__

.export NO_PRODOS_ERROR
NO_PRODOS_ERROR:
    lda   #<NO_PRODOS_STR
    ldx   #>NO_PRODOS_STR
    jsr   PRINT_H

    jsr   ROM_RDKEY
    jmp   QUIT_PRODOS_H

.export PRINT_H
PRINT_H:
    sta   PTRL
    stx   PTRH
    ldx   VERSION
    ldy   #$00
:   lda   (PTRL),y
    beq   :++
    cpx   #$06            ; //e ?
    beq   :+
    cmp   #$60            ; lowercase ?
    bcc   :+
    and   #$5F            ; -> uppercase
:   ora   #$80
    jsr   ROM_COUT
    iny
    bne   :--             ; Branch always
:   rts

.export CLEAR_PATHNAME
CLEAR_PATHNAME:
    ; clear out PATHNAME
    ldx   #0
    lda   #0
  clear_loop:
    sta   PATHNAME, x
    inx
    bpl   clear_loop
    rts

.export DETECT_PRODOS
DETECT_PRODOS:
    Snippet_DETECT_PRODOS
    rts

.export DETECT_BASIC_SYSTEM
DETECT_BASIC_SYSTEM:
    Snippet_DETECT_BASIC_SYSTEM
    rts

.export QUIT_PRODOS_H
QUIT_PRODOS_H:
    jsr   MLI
    .byte CALL_QUIT
    .word QUIT_PARAM_H


; -----------------------------------------------------------------------------------------------
;                                          HIGH SIDE DATA
; -----------------------------------------------------------------------------------------------

.segment "DATA_H"

.export LOADING_STR
LOADING_STR:
  .asciiz "LOADING..."

.export NO_PRODOS_STR
NO_PRODOS_STR:
  .asciiz "THIS PROGRAM REQUIRES PRODOS TO RUN!"

.export SET_PREFIX_PARAM
SET_PREFIX_PARAM:
    .byte $01
    .addr PATHNAME

.export ON_LINE_PARAM
ON_LINE_PARAM:
    .byte $02         ; PARAM_COUNT
ON_LINE_UNIT_NUM:
    .byte $00         ; UNIT_NUM
    .addr PATHNAME+1  ; DATA_BUFFER

.export QUIT_PARAM_H
QUIT_PARAM_H:
    .byte $04             ;PARAM_COUNT
    .byte $00             ;QUIT_TYPE
    .word $0000           ;RESERVED
    .byte $00             ;RESERVED
    .word $0000           ;RESERVED


; -----------------------------------------------------------------------------------------------
;                                     LOW SIDE CODE
; -----------------------------------------------------------------------------------------------

.segment "CODE_L"
    ; code to run on low side
    ; if basic is running, adjust the loading buffer (move it down one page)
    ;lda    BASIC_FLAG
    ;beq    :+
    ; lda   #<(MLI - 512)
  ;   sta   OPEN_PARAM_BUFFER_ADDR
;     lda   #>(MLI - 512)
;     sta   OPEN_PARAM_BUFFER_ADDR+1
; :

    ; loop through file list, starting at last and work towards beginning
    ldx   #(MODULE_ENTRIES - 1) * 16
  module_loop:
    ; copy filename to PATHNAME
    lda   FILE_DATA, x
    beq   skip

    txa   ; save x for loop
    pha

    stx   TEMP  ; save offset into FILE_DATA

    lda   #<FILE_DATA
    sta   PTRL
    lda   #>FILE_DATA
    sta   PTRH

    Util_Inc_16_Addr PTRL, TEMP  ; increment FILE_DATA ptr to current record

    lda   FILE_DATA, x  ; load in actual string len
    sta   PATHNAME      ; save to pathname
    tax
    txa
    tay   ; move length to y
    lda   #0
    sta   PATHNAME+1, x ; write \0 at end of string

  char_loop:
    lda   (PTRL), y
    sta   PATHNAME, x
    dey
    dex
    bpl   char_loop

    ; first do file_info to get aux load addr
    jsr   MLI
    .byte CALL_GET_FILE_INFO
    .word GET_FILE_INFO_PARAM
    bcc   :+
    jmp   ERROR
  :

    ; now, open the file
    jsr   MLI
    .byte CALL_OPEN
    .word OPEN_PARAM
    nop
    bcc   :+
    jmp   ERROR
  :

    ; copy file refnum over to read/close calls
    lda   OPEN_REF
    sta   CLOSE_REF
    sta   READ_REF

    lda   FILE_INFO_ADDR
    ldx   FILE_INFO_ADDR+1
    sta   READ_ADDR
    stx   READ_ADDR+1

    jsr   MLI
    .byte CALL_READ
    .word READ_PARAM
    bcs   ERROR

    jsr   MLI
    .byte CALL_CLOSE
    .word CLOSE_PARAM
    bcs   ERROR

    pla   ; restore x
    tax

skip:
    txa
    ; subtract 16 from x, as each record is 16 bytes
    sec
    sbc   #16
    tax
    ; if not zero, loop back
    bmi   :+
    jmp   module_loop
  : 

    ; now jump to the last module we loaded, which should be the start section of code
    jmp   (READ_ADDR)

QUIT_PRODOS:
    jsr   MLI
    .byte CALL_QUIT
    .word  QUIT_PARAM

PRINT_L:
    sta   PTRL
    stx   PTRH
    ldx   VERSION
    ldy   #$00
:   lda   (PTRL),y
    beq   :++
    cpx   #$06            ; //e ?
    beq   :+
    cmp   #$60            ; lowercase ?
    bcc   :+
    and   #$5F            ; -> uppercase
:   ora   #$80
    jsr   ROM_COUT
    iny
    bne   :--             ; Branch always
:   rts

.export ERROR
ERROR:
    pha
    lda   #<ERROR_STR
    ldx   #>ERROR_STR
    jsr   PRINT_L
    pla
    jsr   ROM_PRBYTE
    lda   #$20 + $80
    jsr   ROM_COUT
    lda   #<PATHNAME+1
    ldx   #>PATHNAME
    jsr   PRINT_L
    jsr   ROM_RDKEY
    jmp   QUIT_PRODOS


; -----------------------------------------------------------------------------------------------
;                                        LOW SIDE DATA
; -----------------------------------------------------------------------------------------------

.segment "DATA_L"

.export ERROR_STR

ERROR_STR:
  .asciiz "ERR: $"

.export FILE_INFO_ADDR, GET_FILE_INFO_PARAM, OPEN_PARAM, OPEN_REF, READ_PARAM, READ_REF, READ_ADDR, CLOSE_PARAM, CLOSE_REF, QUIT_PARAM, FILE_DATA

GET_FILE_INFO_PARAM:
    .byte $0A             ;PARAM_COUNT
    .addr PATHNAME        ;PATHNAME
    .byte $00             ;ACCESS
    .byte $00             ;FILE_TYPE
FILE_INFO_ADDR: 
    .word $0000           ;AUX_TYPE
    .byte $00             ;STORAGE_TYPE
    .word $0000           ;BLOCKS_USED
    .word $0000           ;MOD_DATE
    .word $0000           ;MOD_TIME
    .word $0000           ;CREATE_DATE
    .word $0000           ;CREATE_TIME

OPEN_PARAM:
    .byte $03             ;PARAM_COUNT
    .addr PATHNAME        ;PATHNAME
OPEN_PARAM_BUFFER_ADDR:
    .addr IO_BUFFER_ADDR  ;IO_BUFFER
OPEN_REF:
    .byte $00             ;REF_NUM

READ_PARAM:
    .byte $04             ;PARAM_COUNT
READ_REF:
    .byte $00             ;REF_NUM
READ_ADDR:
    .addr $0000           ;DATA_BUFFER
    .word $FFFF           ;REQUEST_COUNT
    .word $0000           ;TRANS_COUNT

CLOSE_PARAM:
    .byte $01             ;PARAM_COUNT
CLOSE_REF:
    .byte $00             ;REF_NUM

QUIT_PARAM:
    .byte $04             ;PARAM_COUNT
    .byte $00             ;QUIT_TYPE
    .word $0000           ;RESERVED
    .byte $00             ;RESERVED
    .word $0000           ;RESERVED

;.define TESTFILENAME "BASIC.SYSTEM"
.define TESTFILENAME "COPYIIPLUS.8.4"

FILE_DATA:
    .byte .strlen(TESTFILENAME)
    .byte TESTFILENAME
    .res 15 - .strlen(TESTFILENAME)
    .repeat 4
    .byte $00
    .byte "123456789012345"
    .endrepeat

