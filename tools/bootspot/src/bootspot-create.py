import sys, os, math
import argparse
import shutil

# takes a copy of the compiled bootspot and modifies it to contain references to the binaries to load


def main():
  parser = argparse.ArgumentParser(description="Copies and modifies a custom Bootspot loader")
  parser.add_argument('--bins', required=True)
  parser.add_argument('--out', required=True)
  args = parser.parse_args()

  curr_path = os.path.abspath(os.path.dirname(sys.argv[0]))
  src_path = os.path.join(curr_path, '../build/')
  
  #print(args)
  #print(curr_path, src_path)

  bin_list = args.bins.split(',')

  record_size = 15 + 1

  # build data to write to file
  str_data = bytearray((record_size) * 5)

  #print (bin_list)

  curr_count = 0
  for bin_name in bin_list:    
    idx = curr_count * record_size
    print("working on {} at idx={}".format(bin_name, idx))
    str_data[idx] = len(bin_name)
    for c_idx in range(0, len(bin_name)):
      str_data[idx + 1 + c_idx] = ord(bin_name[c_idx])
    curr_count += 1


  #print (list(str_data))

    shutil.copy2(os.path.join(src_path, 'BOOTSPOT.bin'), args.out)

  with open(args.out, 'rb') as bootspot:
    data = bytearray(bootspot.read())

  # modify the bootspot loader
  offset = len(data) - len(str_data)
  for d_idx in range(0, len(str_data)):
    data[offset + d_idx] = str_data[d_idx]

  with open(args.out, 'wb') as bootspot:
    bootspot.write(data)


  sys.exit()



if __name__ == "__main__":
  main()

