# be careful, any BINFILE name must be 8 chars max

CURR_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
BUILDDST=$(CURR_PATH)build

BINFILE = BOOTSPOT
BINFILE_PATH = $(BUILDDST)
BINFILE_FULL = $(BINFILE_PATH)/$(BINFILE).bin
OBJS = main.o
LIBS = 
TARGET_ARCH = apple2
TARGET_MEMORY_MAP = src/memory-64k.cfg

OBJ_PATH = $(BUILDDST)/obj

include ../../binasm.mk
include ../../diskpack.mk
include ../../assettools.mk

COMMON_INCLUDE = $(INCLUDE_PATH)/utils.inc

CPU_TYPE = 6502

PRODOS_BOOT_DISK = $(BUILDDST)/ProDOS_2_4_2.dsk

$(BUILDDST)/obj/main.o: src/main.s $(COMMON_INCLUDE)
	$(CA65) $(AFLAGS) $< -o $@

test:
	-../../emu/AppleWin/Applewin.exe -no-printscreen-key -d1 $(CURR_PATH)/$(PRODOS_BOOT_DISK) -s7 empty

.PHONY: all clean cleanassets assets dskHDD cleandsk dsk test cleantxt mount music cleanmusic test testimages

push:
	-cp 

all: | bin

clean: cleanbin
